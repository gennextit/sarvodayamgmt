package com.gennext.sarvodayamgmt;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;

public class CompanyActivity extends NavDrawer{

    private Toolbar mToolbar;
    private ImageButton ibNotification,ibHome;
    private CoordinatorLayout cLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar=setToolBar(getSt(R.string.app_name));
        SetDrawer(this,mToolbar);
        initUi();
    }


    private void initUi() {

    }


}
