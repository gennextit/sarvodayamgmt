package com.gennext.sarvodayamgmt.util;


import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class ApiCall {
    public static final String IO_EXCEPTION = "IOException";

    //GET network request
    public static String GET(String url, String params){
        return GET(url+params);
    }
    public static String GET(String url, List<KeyValuePair> params){
        return GET(url,params,false);
    }
    public static String GET(String url, List<KeyValuePair> params, boolean hasEncoding){
        String paramString = format2(params,hasEncoding ? "utf-8":null);
        String finalUrl = url + "?" + paramString;
        return GET(finalUrl);
    }
    public static String GET(String url){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        }catch (IOException e){
            return IO_EXCEPTION+" "+e.toString();
        }
    }

    public static String POST(String url, List<KeyValuePair> params) {
        return POST(url,getParams(params));
    }

    //POST network request
    public static String POST(String url, RequestBody body){
        String res = null;
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
        // socket timeout
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            res=response.body().string();
        } catch (IOException e) {
            return IO_EXCEPTION+" "+e.toString();
        }
        return res;
    }

    public static String upload(String url, RequestBody formBody){
        OkHttpClient client = new OkHttpClient();
        try {
            Request request = new Request.Builder().url(url).post(formBody).build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return IO_EXCEPTION+" "+e.toString();
        }
    }

    public static RequestBody getParams(List<KeyValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (KeyValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
        return builder.build();
    }

    public static String getAssets(Context context, String fileName) {
        String json;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return ex.toString();
        }
        return json;
    }
    public static String format2(List<KeyValuePair> list, String encoding) {
        StringBuilder result = new StringBuilder();

        for (KeyValuePair parameter:list){
            String encodedValue;
            String encodedKey;
            if(encoding!=null){
                encodedKey = encode(parameter.getKey(), encoding);
                encodedValue = parameter.getValue() != null?encode(parameter.getValue(), encoding):"";
            }else{
                encodedKey = parameter.getKey();
                encodedValue = parameter.getValue();
            }
            if(result.length() > 0) {
                result.append("&");
            }

            result.append(encodedKey);
            result.append("=");
            result.append(encodedValue);
        }
        return result.toString();
    }

    public static String format(List<KeyValuePair> parameters, String encoding) {
        StringBuilder result = new StringBuilder();
        Iterator i$ = parameters.iterator();

        while(i$.hasNext()) {
            KeyValuePair parameter = (KeyValuePair)i$.next();
            String encodedKey = encode(parameter.getKey(), encoding);
            String value = parameter.getValue();
            String encodedValue = value != null?encode(value, encoding):"";
            if(result.length() > 0) {
                result.append("&");
            }

            result.append(encodedKey);
            result.append("=");
            result.append(encodedValue);
        }

        return result.toString();
    }

    private static String decode(String content, String encoding) {
        try {
            return URLDecoder.decode(content, encoding != null?encoding:"ISO-8859-1");
        } catch (UnsupportedEncodingException var3) {
            throw new IllegalArgumentException(var3);
        }
    }

    private static String encode(String content, String encoding) {
        try {
            return URLEncoder.encode(content, encoding != null?encoding:"ISO-8859-1");
        } catch (UnsupportedEncodingException var3) {
            throw new IllegalArgumentException(var3);
        }
    }
}