package com.gennext.sarvodayamgmt.util;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gennext.sarvodayamgmt.model.LoginModel;
import com.gennext.sarvodayamgmt.setting.Const;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.gennext.sarvodayamgmt.util.DBHelperConst.KEY_CREATED_AT;
import static com.gennext.sarvodayamgmt.util.DBHelperConst.KEY_EMAIL;
import static com.gennext.sarvodayamgmt.util.DBHelperConst.KEY_ID;
import static com.gennext.sarvodayamgmt.util.DBHelperConst.KEY_PASSWORD;
import static com.gennext.sarvodayamgmt.util.DBHelperConst.KEY_USER_ID;


public class DBHelper extends SQLiteOpenHelper {
    // Logcat tag
    private static final String LOG = "DBHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = AppUser.COMMON + "_DB";
    private final SQLiteDatabase db;
    // Table Names
    private static final String TABLE_LOGIN = "login";


    // Table Create Statements
    // Todo table create statement

    private static final String CREATE_LOGIN = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LOGIN + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_USER_ID + " TEXT," + KEY_EMAIL + " TEXT," + KEY_PASSWORD + " TEXT," + KEY_CREATED_AT
            + " DATETIME" + ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_LOGIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);
        // create new tables
        onCreate(db);
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    // ------------------------ "todos" table methods ----------------//


    public LoginModel addNewMember(String userId, LoginModel result) {
        if (result == null) {
            result = new LoginModel();
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Member detail doesn't exist");
            return result;
        }
        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_PASSWORD + " = '" + result.getPassword() + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (!c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_ID, userId);
            values.put(KEY_EMAIL, result.getEmail());
            values.put(KEY_PASSWORD, result.getPassword());
            values.put(KEY_CREATED_AT, getDateTime());

            // insert row
            long todo_id = db.insert(TABLE_LOGIN, null, values);
            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_INSERTED);
            result.setId(String.valueOf(todo_id));
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Member mobile already exist");
            return result;
        }
    }

    public LoginModel updateNewMember(String userId, LoginModel result) {

        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN
                + " WHERE " + KEY_ID + " = '" + result.getId() + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_ID, userId);
            values.put(KEY_EMAIL, result.getEmail());
            values.put(KEY_PASSWORD, result.getPassword());
            values.put(KEY_CREATED_AT, getDateTime());
            // updating row
            int res = db.update(TABLE_LOGIN, values, KEY_ID + " = ? AND " + KEY_USER_ID + " = ?",
                    new String[]{result.getId(), userId});

            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_UPDATED);
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Employee does not exist");
            return result;
        }
    }

    public LoginModel getAllNewMember(String userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
//		SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        LoginModel result = new LoginModel();
        result.setList(new ArrayList<LoginModel>());
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            do {
                LoginModel model = new LoginModel();
                model.setId(c.getString(c.getColumnIndex(KEY_ID)));
                model.setUserId(c.getString(c.getColumnIndex(KEY_USER_ID)));
                model.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
                model.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
                model.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
                result.getList().add(model);
            } while (c.moveToNext());
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        return result;
    }

    public LoginModel deleteNewMember(String userId, String id) {
        LoginModel result = new LoginModel();
        int temp = db.delete(TABLE_LOGIN, KEY_ID + " = ? AND " + KEY_USER_ID + " = ?",
                new String[]{id, userId});
        if (temp == 1) {
            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_DELETED_SUCCESSFUL);
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        return result;
    }

    public LoginModel memberLogin(String userId, String password) {
        LoginModel result = new LoginModel();

        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_PASSWORD + " = '" + password + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            result.setId(c.getString(c.getColumnIndex(KEY_ID)));
            result.setId(c.getString(c.getColumnIndex(KEY_ID)));
            result.setUserId(c.getString(c.getColumnIndex(KEY_USER_ID)));
            result.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
            result.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
            result.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Login credentials doesn't match");
            return result;
        }
    }
 
}


