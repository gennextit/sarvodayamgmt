package com.gennext.sarvodayamgmt.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Abhijit on 16-Dec-16.
 */

public class Rating {
    private static final String APP_NAME=AppUser.COMMON;
    private static final String RATING_COUNTER="rc"+APP_NAME;
    private static final String APP_COUNTER="app"+APP_NAME;
    private static final String REMIND_INTERVAL="riterval"+APP_NAME;
    private static final String RATING_PROVIDED = "rprovid"+APP_NAME;
    private static final String RATING_INTEREST_LEVEL = "interestLev"+APP_NAME;
    private static int remindInterval=4;
    private static int ratingCounter=4;
    private static int ratingInterestLevel=4;

    public static boolean showRating(Context context) {
        int rInterval=getRemindInterval(context);
        if(rInterval<=remindInterval){
            setAppCounter(context);
            int appCount=getAppCounter(context);
            int rCounter=getRatingCounter(context);
            if (appCount==rCounter){
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }

    public static void setAppCounter(Context context) {
        int current=getAppCounter(context);
        current++;
        SavePrefInt(context, APP_COUNTER, current);
    }
    public static int getAppCounter(Context context) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            int data = sharedPreferences.getInt(APP_COUNTER, 1);
            return data;
        }else{
            return 0;
        }
    }

    public static void setRatingCounter(Context context, int counter) {
        SavePrefInt(context, RATING_COUNTER, counter);
    }
    public static int getRatingCounter(Context context) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            int data = sharedPreferences.getInt(RATING_COUNTER, ratingCounter);
            return data;
        }else{
            return 0;
        }
    }

    public static void remindMeLater(Context context) {
        int rLeter=getRatingCounter(context);
        setRatingCounter(context,rLeter+ratingCounter);
        setRemindInterval(context);
    }

    public static void stopRating(Context context) {
        SavePrefInt(context, REMIND_INTERVAL, 100);
    }

    public static void setRemindInterval(Context context) {
        int rInterval=getRemindInterval(context);
        rInterval++;
        SavePrefInt(context, REMIND_INTERVAL, rInterval);
    }
    public static int getRemindInterval(Context context) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            int data = sharedPreferences.getInt(REMIND_INTERVAL, 1);
            return data;
        }else{
            return 0;
        }
    }

    public static void SavePrefInt(Context context, String key, int value) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.apply();
        }
    }

    public static int LoadPrefInt(Context context, String key, int defaultValue) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getInt(key, defaultValue);
        }else{
            return defaultValue;
        }
    }

    public static void setRatingProvided(Context context, Boolean status) {
        Utility.SavePrefBoolean(context,RATING_PROVIDED,status);
    }

    public static boolean isRatingProvidedByUser(Context context) {
        return Utility.LoadPrefBoolean(context,RATING_PROVIDED,false);
    }

    public static int getInterestLevel(Context context) {
        return LoadPrefInt(context,RATING_INTEREST_LEVEL,1);
    }

    public static void setInterestLevel(Context context) {
        int rInterval=getInterestLevel(context);
        rInterval++;
        SavePrefInt(context, RATING_INTEREST_LEVEL, rInterval);
    }


}
