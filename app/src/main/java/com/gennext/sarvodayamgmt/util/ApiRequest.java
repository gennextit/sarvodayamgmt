package com.gennext.sarvodayamgmt.util;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created by Admin on 3/8/2018.
 */

public class ApiRequest {

    private final Listener mListener;
    private Activity activity;
    private String url;
    private int method;
    public static final int GET=1,POST=2;
    private List<KeyValuePair> params;

    public interface Listener {
        void onResponse(String response);
        void onErrorResponse(String error);
    }

    public ApiRequest(Activity activity, Listener listener) {
        this.activity = activity;
        this.mListener = listener;
    }

    public void execute(int method, String url, List<KeyValuePair> params) {
        this.method = method;
        this.url = url;
        this.params = params;
        new ApiRequestTask().execute(url);
    }

    public void execute(String url) {
        this.method = GET;
        this.url = url;
        new ApiRequestTask().execute(url);
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        this.activity = null;
    }


    private class ApiRequestTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            if(method==GET){
                if(params==null){
                    return ApiCall.GET(url[0]);
                }else{
                    return ApiCall.GET(url[0],params);
                }
            }else{
                return ApiCall.POST(url[0],params);
            }
        }

        @Override
        protected void onPostExecute(String response) {
            // TODO Auto-generated method stub
            super.onPostExecute(response);
            if (activity != null) {
                if (response != null) {
                    if (response.contains(ApiCall.IO_EXCEPTION)) {
                        mListener.onErrorResponse(response);
                    } else {
                        mListener.onResponse(response);
                    }
                }
            }
        }

    }
}
