package com.gennext.sarvodayamgmt.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtility {


    public static String convertTime24to12Hours(String inputTime) {
        DateFormat inputFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertTime12to24Hours(String inputTime) {
        DateFormat inputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static int convertTimeStamp(String input12Time) {
        DateFormat inputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("HHmmss", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(input12Time);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return parseInt(outputDateStr);
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }


    public static String[] getHourMinute24(String inputTime) {
        DateFormat inputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            return new String[]{"",""};
        }
        return outputDateStr.split(":");
    }

    public static String convertDateMMM(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDate(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDateMMM2(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDateYMD(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDateYMD2(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String serverFormat(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDateYMD3(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDateSplit(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertDate(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
//		int numDays = cal.getActualMaximum(Calendar.DATE);
        String format = new SimpleDateFormat("d MMMM yyyy, EEEE", Locale.US).format(cal.getTime());

        return format;
    }

    public static String cDateDDMMMYY(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String format = new SimpleDateFormat("dd-MMM-yy", Locale.US).format(cal.getTime());

        return format;
    }

    public static String cDateMDY(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String format = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(cal.getTime());

        return format;
    }

    public static String cViewDateMDY(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String format = new SimpleDateFormat("d MMMM yyyy, EEEE", Locale.US).format(cal.getTime());

        return format;
    }

    public static int convertDateStamp(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String format = new SimpleDateFormat("yyyyMMdd", Locale.US).format(cal.getTime());

        return Integer.parseInt(format);
    }

    public static String convertDateTimeStamp(int year, int month, int day, int hour, int min) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);

        String format = new SimpleDateFormat("yyyyMMddHHmm", Locale.US).format(cal.getTime());

        return format;
    }

    public static String convertDateTimeStamp(String inp, int hour, int min) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inp);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
        }
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);

        String format = new SimpleDateFormat("HHmm", Locale.US).format(cal.getTime());

        return outputDateStr+format;
    }

    public static String convertDateTimeStamp(String inputDate, String time) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyhh:mm a", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate+time);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertTimeTo24Hours(int hour, int min, String AMPM) {
        String startTime = String.valueOf(hour) + ":" + String.valueOf(min) + ":" + AMPM.toLowerCase();
        DateFormat inputFormat = new SimpleDateFormat("hh:mm:a", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(startTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    // enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
    // enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
    public static String convertFormateDate(String Date, int type, String dateFormat) {
        String Day, middle, Month, Year;
        String finalDate = Date;
        if (type == 1) {
            Day = Date.substring(0, 2);
            middle = Date.substring(2, 3);
            Month = Date.substring(3, 5);
            Year = Date.substring(6, 10);

        } else {
            Day = Date.substring(0, 4);
            middle = Date.substring(4, 5);
            Month = Date.substring(5, 7);
            Year = Date.substring(8, 10);
        }

        switch (dateFormat) {
            case "dd-MM-yyyy":
                finalDate = Day + middle + Month + middle + Year;
                break;
            case "yyyy-MM-dd":
                finalDate = Year + middle + Month + middle + Day;
                break;
            case "MM-dd-yyyy":
                finalDate = Month + middle + Day + middle + Year;
                break;
            default:
                finalDate = "Date Format Incorrest";
        }
        return finalDate;
    }

    public static String convertTime(String time) {
        // String s = "12:18:00";
        String[] res = time.split(":");
        int hr = Integer.parseInt(res[0]);
        String min = res[1];

        if (hr == 12) {
            return (12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
        }

        return (hr % 12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
    }

    public void getDateTime() {
        String date;
        final Calendar cal = Calendar.getInstance();
//		syear = cal.get(Calendar.YEAR);
//		smonth = cal.get(Calendar.MONTH);
//		sday = cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int generateTimeStamp(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String format = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(cal.getTime());
        String[] tempDate = format.split("-");
        String timeStamp = String.valueOf(tempDate[2]) + String.valueOf(tempDate[1]) + String.valueOf(tempDate[0]);
        return Integer.parseInt(timeStamp);
    }

    public static int generateTimeStamp(String date) {
        String[] tempDate = convertDateSplit(date).split("-");
        String timeStamp = String.valueOf(tempDate[2]) + String.valueOf(tempDate[1]) + String.valueOf(tempDate[0]);
        return Integer.parseInt(timeStamp);
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat("_yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    }

    public static String getDateStampFormatted() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    public static int[] getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        return new int[]{day, month, year};
    }

    public static String getDateTimeStampFormatted() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public static String[] getMonthStampFormatted() {
        String[] temp = new String[2];
        String month = new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());
        temp[0] = month;
        Calendar cal = Calendar.getInstance();
        int numDays = cal.getActualMaximum(Calendar.DATE);
        temp[1] = String.valueOf(numDays);
        return temp;
    }

    public static String getTimeStampFormatted() {
        return new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public static String convertSerevrDate(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static String convertSerevrDateTime(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy hh:mm:a", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
            outputDateStr="Not available";
        }
        return outputDateStr;
    }


    public static String[] splitTime(String startTime) {
        DateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("hh:mm:a", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(startTime);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr.split(":");
    }

    public static String getYearMonth() {
        return new SimpleDateFormat("yyyy-MM", Locale.getDefault()).format(new Date());
    }

    public static String getYearMonth(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM", Locale.US);
        Date date = null;
        String outputDateStr = "";
        try {
            date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException | NullPointerException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    public static int[] getDayMonthYear(String inputDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy", Locale.US);
        Date date = null;
        try {
            date = format.parse(inputDate);
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.setTime(date);
            int sYear = calendar.get(Calendar.YEAR);
            int sMonth = calendar.get(Calendar.MONTH);
            int sDay = calendar.get(Calendar.DAY_OF_MONTH);
            return new int[]{sDay, sMonth, sYear};
        } catch (ParseException e) {
            L.m(e.toString());
            return new int[]{0, 0, 0};
        }
//        System.out.println(calendar.get(Calendar.YEAR));
//        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
//        System.out.println(new SimpleDateFormat("MMM").format(calendar.getTime()));
    }

    public static ArrayList<Date> getDateFromRange(String startDate, String endDate) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1 .parse(startDate);
            date2 = df1 .parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

//	public static int generateTimeStamp2(String date) {
//		String[] tempDate = convertDateSplit(date).split("-");
//		String timeStamp=String.valueOf(tempDate[2])+String.valueOf(tempDate[1])+String.valueOf(tempDate[0]);
//		return Integer.parseInt(timeStamp);
//	}
}
