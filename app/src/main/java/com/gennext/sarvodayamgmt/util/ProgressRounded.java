package com.gennext.sarvodayamgmt.util;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.gennext.sarvodayamgmt.R;


/**
 * Created by Admin on 8/28/2017.
 */

public class ProgressRounded {

    private final ProgressBar pBar;
    private final Animation inAnim,outAnim;
//    private final ImageView ivStatus;

    public static ProgressRounded newInstance(Context context, View v) {
        return new ProgressRounded(context, v);
    }

    public ProgressRounded(Context context, View view) {
//        ivStatus = (ImageView) view.findViewById(R.id.iv_status);
        pBar = (ProgressBar) view.findViewById(R.id.progressBar);
        inAnim = AnimationUtils.loadAnimation(context,
                android.R.anim.fade_in);
        outAnim = AnimationUtils.loadAnimation(context,
                android.R.anim.fade_out);

    }

    public void showProgressBar() {
        if (pBar != null) {
            pBar.startAnimation(inAnim);
            pBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if (pBar != null) {
            pBar.startAnimation(outAnim);
            pBar.setVisibility(View.GONE);
        }
    }

}
