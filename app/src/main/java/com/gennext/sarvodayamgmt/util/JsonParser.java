package com.gennext.sarvodayamgmt.util;



import android.text.TextUtils;

import com.gennext.sarvodayamgmt.model.AppointmentModel;
import com.gennext.sarvodayamgmt.model.CompanyModel;
import com.gennext.sarvodayamgmt.model.DoctorModel;
import com.gennext.sarvodayamgmt.model.EmployeesModel;
import com.gennext.sarvodayamgmt.model.Model;
import com.gennext.sarvodayamgmt.pannel.DepartmentSelector;
import com.gennext.sarvodayamgmt.pannel.DoctorSelector;
import com.gennext.sarvodayamgmt.setting.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";
    public static final String DEFAULT_ERROR_CODE = "ERROR-101";
    public static final String DEFAULT_ERROR_MSG = "ERROR-101 No msg available";
    public static final String SERVER = ApiCallError.SERVER_ERROR;
    public static final String INTERNET = ApiCallError.INTERNET_ERROR;
    public static final String IO_EXCEPTION = ApiCall.IO_EXCEPTION;
    public static final String JSON_ERROR = ApiCallError.JSON_ERROR;

    //default constant variable
    public static final String ERROR_CODE = "ErrorCode";
    public static final String ERROR_MSG = "ErrorMsg";


    public static Model defaultSuccessResponse(String response) {
        Model model = new Model();
        model.setOutput(Const.SUCCESS);
        model.setOutputMsg("Attendance Marked successful");
        return model;
    }

    public static Model defaultParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(DEFAULT_ERROR_CODE);
        jsonModel.setOutputMsg(DEFAULT_ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (!TextUtils.isEmpty(mainObject.optString(ERROR_CODE))) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(ERROR_MSG));
                    } else {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(ERROR_MSG));
                    }

                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    private static String[] defaultParserModel(String response) {
        String output = "", outputMsg = "";
        if (response.contains(IO_EXCEPTION)) {
            output = INTERNET;
            outputMsg = response;
            return new String[]{output, outputMsg};
        } else {
            if (response.contains("[")) {
                String finalResult = response.substring(response.indexOf('['), response.indexOf(']') + 1);
                try {
                    JSONArray mainArray = new JSONArray(finalResult);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (!TextUtils.isEmpty(mainObject.optString(ERROR_CODE))) {
                        output = Const.FAILURE;
                        outputMsg = mainObject.getString(ERROR_MSG);
                    } else {
                        output = Const.SUCCESS;
                        outputMsg = mainArray.toString();
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    output = SERVER;
                    outputMsg = e.toString() + "\n" + response;
                }
            } else {
                ERRORMESSAGE = response;
                output = SERVER;
                outputMsg = response;
            }
        }
        return new String[]{output, outputMsg};
    }

    public static Model manageDefault(String response) {
        Model model = new Model();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if(!model.getOutput().equals(Const.SUCCESS)){
            return model;
        }
        //write your logic here for success response
//        model.setList(new ArrayList<Model>());
//        try {
//            JSONArray mainArray=new JSONArray(model.getOutputMsg());
//            for (int i=0;i<mainArray.length();i++){
//                JSONObject mainObj=mainArray.getJSONObject(i);
//                CompanyModel item = new CompanyModel();
//                item.setId(mainObj.optString("id"));
//                item.setName(mainObj.optString("companyName"));
//                item.setEmail(mainObj.optString("email"));
//                model.getList().add(item);
//            }
//
//        } catch (JSONException e) {
//            model.setOutput(JSON_ERROR);
//            model.setOutputMsg(e.toString() + "\n" + response);
//        }
        return model;
    }

    public static CompanyModel companyLogin(String response) {
        return null;
    }

    public static EmployeesModel employeeLogin(String response, DBHelper dbManager) {
        return null;
    }

    public static DoctorModel getDoctorList(String response) {
        DoctorModel model = new DoctorModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if(!model.getOutput().equals(Const.SUCCESS)){
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<DoctorModel>());
        DoctorModel item2 = new DoctorModel();
        item2.setDoctorId(DoctorSelector.ALL_DOCTOR_ID);
        item2.setDoctorName("All Doctors");
        item2.setDepartment(DepartmentSelector.ALL_DEPARTMENT);
        model.getList().add(item2);
        try {
            JSONArray mainArray=new JSONArray(model.getOutputMsg());
            for (int i=0;i<mainArray.length();i++){
                JSONObject mainObj=mainArray.getJSONObject(i);
                DoctorModel item = new DoctorModel();
                item.setDoctorId(mainObj.optString("Doctor_ID"));
                item.setDoctorName(mainObj.optString("DoctorName"));
                item.setDepartment(mainObj.optString("Department"));
                item.setSpecialization(mainObj.optString("Specialization"));
                item.setDegree(mainObj.optString("Degree"));
                item.setImageURL(mainObj.optString("ImageURL"));
                model.getList().add(item);
            }

        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static DoctorModel getDepartmentList(String response) {
        DoctorModel model = new DoctorModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if(!model.getOutput().equals(Const.SUCCESS)){
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<DoctorModel>());
        DoctorModel item2 = new DoctorModel();
        item2.setDepartment(DepartmentSelector.ALL_DEPARTMENT);
        model.getList().add(item2);
        try {
            JSONArray mainArray=new JSONArray(model.getOutputMsg());
            for (int i=0;i<mainArray.length();i++){
                JSONObject mainObj=mainArray.getJSONObject(i);
                DoctorModel item = new DoctorModel();
                item.setDepartment(mainObj.optString("Department"));
                model.getList().add(item);
            }

        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static CompanyModel companySignup(String response) {
        return null;
    }

    public static AppointmentModel parseLoadAppointmentByDoctor(AppointmentModel dateModel, String response, DoctorModel mDoctor) {

        String[] result = defaultParserModel(response);
        dateModel.setOutputMsg(result[1]);
        dateModel.setOutput(result[0]);
        if(!result[0].equals(Const.SUCCESS)){
//            dateModel.setTotal("0");
            return dateModel;
        }
        try {
            JSONArray mainArray=new JSONArray(dateModel.getOutputMsg());
//            dateModel.setTotal(String.valueOf(mainArray.length()));
            for (int i=0;i<mainArray.length();i++){
                JSONObject mainObj=mainArray.getJSONObject(i);
                AppointmentModel item = new AppointmentModel();
                item.setAppId(mainObj.optString("App_ID"));
                item.setAppDate(mainObj.optString("Appointment_Date"));
                item.setAppTime(mainObj.optString("Appointment_Time"));
                item.setDoctor(mDoctor);
                dateModel.getAppList().add(item);
            }
        } catch (JSONException e) {
//            dateModel.setTotal("0");
            dateModel.setOutput(JSON_ERROR);
            dateModel.setOutputMsg(e.toString());
        }
        return dateModel;
    }

    public static DoctorModel mergeDoctorAndDepartment(DoctorModel department, DoctorModel doctor) {
        for (DoctorModel dep : department.getList()){
            dep.setDoctorList(new ArrayList<DoctorModel>());
            for (DoctorModel doc : doctor.getList()){
                if(dep.getDepartment().equalsIgnoreCase(doc.getDepartment())){
                    dep.getDoctorList().add(doc);
                }
            }
        }
        return department;
    }

    public static AppointmentModel getAllTodayAppointments(String response) {
        AppointmentModel model =new AppointmentModel();
        String[] result = defaultParserModel(response);
        model.setOutputMsg(result[1]);
        model.setOutput(result[0]);
        model.setAppList(new ArrayList<AppointmentModel>());
        double totalPayment = 0.0;
        int unpaidCoint=0;
        if(!result[0].equals(Const.SUCCESS)){
            return model;
        }
        try {
            JSONArray mainArray=new JSONArray(model.getOutputMsg());
            for (int i=0;i<mainArray.length();i++){
                JSONObject mainObj=mainArray.getJSONObject(i);
                AppointmentModel item = new AppointmentModel();
                item.setDoctorId(mainObj.optString("DoctorId"));
                item.setDoctorName(mainObj.optString("DoctorName"));
                item.setAppId(mainObj.optString("AppNo"));
                item.setAppTime(mainObj.optString("AppTime"));
                item.setPatientId(mainObj.optString("PatientId"));
                item.setPatientName(mainObj.optString("PatientName"));
                item.setPhone(mainObj.optString("Phone"));
                item.setReceiptNo(mainObj.optString("ReceiptNo"));
                item.setAmount(mainObj.optString("Amount"));
                item.setCallStatus(mainObj.optString("CallStatus"));
                item.setPaymentStatus(mainObj.optString("PaymentStatus"));
                double amount = Utility.parseDouble(mainObj.optString("Amount"));
                totalPayment +=amount;
                if(item.getPaymentStatus().equalsIgnoreCase("unpaid")){
                    unpaidCoint += 1;
                }
                model.getAppList().add(item);
            }
        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString());
        }
        model.setAmount(String.valueOf(totalPayment));
        model.setPaymentStatus(String.valueOf(unpaidCoint));
        return model;
    }



}