package com.gennext.sarvodayamgmt.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Abhijit on 22-Dec-16.
 */

public class WidgetAnimation {

    public static final int ZOOM_IN = 1, ZOOM_IN_BOUNCE = 2, FLIP_HORIZONTAL = 3, FLIP_VERTICAL = 4, HIDE_FLIP_HORIZONTAL = 5, EXPEND = 6, COLLAPSE = 7;
    private static final long ANIM_DURATION_MEDIUM = 500;
    private static final long ANIM_DURATION_LONG = 500;
    private static final long WAITING_TIME_OF_PROGRESS = 1000;

    // zoom in out button animation
    public static void zoomInOutForButton(final View v) {
        Animation anim = new ScaleAnimation(1f, 0.70f, 1f, 0.70f, 50, 50); // Pivot point of Y scaling
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(300);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim = new ScaleAnimation(0.70f, 1f, 0.70f, 1f, 50, 50); // Pivot point of Y scaling
                anim.setInterpolator(new AccelerateInterpolator());
                anim.setDuration(100);
                v.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(anim);
    }


    public static ObjectAnimator setViewAnimation(final View view, int type) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<>();
        ObjectAnimator scaleXAnimator, scaleYAnimator = null;
        switch (type) {
            case ZOOM_IN:
                scaleXAnimator = ObjectAnimator.ofFloat(view, "ScaleX", 0f, 1f, 1f);
                animatorList.add(scaleXAnimator);
                scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 0f, 1f, 1f);
                animatorList.add(scaleYAnimator);
                animatorSet.playTogether(animatorList);
                break;
            case ZOOM_IN_BOUNCE:
                scaleXAnimator = ObjectAnimator.ofFloat(view, "ScaleX", 0f, 1.2f, 1f);
                animatorList.add(scaleXAnimator);
                scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 0f, 1.2f, 1f);
                animatorList.add(scaleYAnimator);
                animatorSet.playTogether(animatorList);
                break;
            case FLIP_VERTICAL:
                scaleXAnimator = ObjectAnimator.ofFloat(view, "ScaleX", 0f, 1.2f, 1f);
                animatorSet.playTogether(scaleXAnimator);
                break;
            case FLIP_HORIZONTAL:
                scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 0f, 1f, 1f);
                animatorSet.playTogether(scaleYAnimator);
//                scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 0f, 1f, 1f);
//                animatorSet.playTogether(scaleYAnimator);
                break;
            case HIDE_FLIP_HORIZONTAL:
                scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, 0f, 0f);
                scaleYAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                animatorSet.playTogether(scaleYAnimator);
                break;
        }
        view.setVisibility(View.VISIBLE);

        animatorSet.start();
        return scaleYAnimator;
    }

    public static ObjectAnimator slideDown(final View view) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = null;
        scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 0f, 1f, 1f);
        animatorSet.playTogether(scaleYAnimator);
        view.setVisibility(View.VISIBLE);
        animatorSet.start();
        return scaleYAnimator;
    }


    public static ObjectAnimator slideUp(final View view) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = null;
        scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, 0f, 0f);
        scaleYAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animatorSet.playTogether(scaleYAnimator);
        animatorSet.start();
        return scaleYAnimator;
    }

    public static void slideLeftIn(final View view) {
        slideLeftIn(view, null);
    }

    public static void slideLeftIn(final View view, Animator.AnimatorListener animationListener) {

//        ObjectAnimator animatorSet =ObjectAnimator.ofFloat(view, "translationX", -100,10,20,30,40,50,60,70,80,90,100,90,80,70,60,50,40,30,20,10,0);
        ObjectAnimator animatorSet = ObjectAnimator.ofFloat(view, "translationX", -500, 50, 0);
        animatorSet.setInterpolator(new AccelerateInterpolator());
        if (animationListener != null)
            animatorSet.addListener(animationListener);
        animatorSet.setDuration(1000);
        animatorSet.start();
    }


    public static void slideRightIn(final View view) {
        slideRightIn(view, null, WAITING_TIME_OF_PROGRESS);
    }

    public static void slideRightIn(final View view, long time) {
        slideRightIn(view, null, time);
    }

    public static void slideRightIn(final View view, final Animator.AnimatorListener animationListener, long waitingTime) {
        view.setVisibility(View.GONE);
        final Handler handler = new Handler();
        final Runnable runnableRevert = new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
//                ObjectAnimator animatorSet = ObjectAnimator.ofFloat(view, "translationX", 500,-50,0);
                ObjectAnimator animatorSet = ObjectAnimator.ofFloat(view, "translationX", 500, 0);
                animatorSet.setInterpolator(new AccelerateInterpolator());
                if (animationListener != null) {
                    animatorSet.addListener(animationListener);
                }
                animatorSet.setDuration(1000);
                animatorSet.start();
            }
        };
        handler.postDelayed(runnableRevert, waitingTime);

    }


    public static void slideLeftOut(final ImageView view) {
        slideLeftOut(view, null);
    }

    public static void slideLeftOut(final ImageView view, Animator.AnimatorListener animationListener) {

        ObjectAnimator animatorSet = ObjectAnimator.ofFloat(view, "translationX", -500);
        animatorSet.setInterpolator(new AccelerateInterpolator());
        if (animationListener != null) {
            animatorSet.addListener(animationListener);
        }
        animatorSet.setDuration(1000);
        animatorSet.start();
    }

    public static void slideRightOut(final View view) {
        slideRightOut(view, null);
    }

    public static void slideRightOut(final View view, Animator.AnimatorListener animationListener) {

        ObjectAnimator animatorSet = ObjectAnimator.ofFloat(view, "translationX", 500,0);
        animatorSet.setInterpolator(new AccelerateInterpolator());
        if (animationListener != null) {
            animatorSet.addListener(animationListener);
        }
        animatorSet.setDuration(1000);
        animatorSet.start();
    }


    public static void expandOrCollapse(final View v, int exp_or_colpse) {
        expandOrCollapse(v, exp_or_colpse, null);
    }


    public static void expandOrCollapse(final View v, int exp_or_colpse, Animation.AnimationListener collapselistener) {
        TranslateAnimation anim = null;
        if (exp_or_colpse == EXPEND) {
            anim = new TranslateAnimation(0.0f, 0.0f, -v.getHeight(), v.getHeight());
            v.setVisibility(View.VISIBLE);
            anim.setAnimationListener(null);

        } else if (exp_or_colpse == COLLAPSE) {
            anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, v.getHeight());
            anim.setAnimationListener(collapselistener);
        }
        // To Collapse
        //

        anim.setDuration(500);
        anim.setInterpolator(new AccelerateInterpolator());
        v.startAnimation(anim);
    }
}
