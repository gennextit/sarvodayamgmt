package com.gennext.sarvodayamgmt.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */

/**
 * ERROR-101 (Unformetted Json Response)
 * ERROR-102 ()
 */
public class AppSettings {

//    public static final String SERVER = "http://upsma.com";
    public static final String COMMON = "http://services.sarvodayahospital.com/mobileApp/mobileapp.asmx/";

    public static final String SEND_FEEDBACK  = COMMON + "sendFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String TERMS_OF_USE  = COMMON + "/files/Terms%20of%20Use.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String PRIVACY_PACY_POLICY  = COMMON + "/files/Privacy%20Policy.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook


    public static String SHARE_URL="http://bdoindia.app.link/category?";//https://www.appsflyer.com
    public static final String REPORT_SERVER_ERROR = COMMON + "serverErrorReporting";
    public static final String LOGIN_COMPANY = COMMON + "";
    public static final String LOGIN_EMPLOYEE = COMMON + "";
    public static final String GET_COMPANIES_LIST = COMMON + "";
    public static final String SIGNUP_COMPANY = COMMON + "";

    public static final String LOAD_DOCTOR = COMMON + "LoadDoctor";
    public static final String LOAD_DEPARTMENT = COMMON + "LoadDepartment";
    public static final String GET_ALL_TODAY_APPOINTMENTS = COMMON + "GetAllAppointments"; // no params
    public static final String VIEW_APPOINTMENT_BY_DOCTOR = COMMON + "ViewAppointmentByDoctor";// Doctorid, AppDate(yyyy-MM-dd)

}
// GetAllAppointments
//{
//        "DoctorId":"LSHHI33",
//        "DoctorName":"Dr.Ashish Gupta/ Varun Randhawa",
//        "AppNo":"A5",
//        "AppTime":"10:20 AM",
//        "PatientId":"369415",
//        "PatientName":"Mrs. KAMLESH DEVI",
//        "Phone":"07835897937",
//        "ReceiptNo":"O/18-19/S8085870",
//        "Amount":0.0,
//        "CallStatus":"uncall",
//        "PaymentStatus":"Paid"
//        }
/* New API's required for BizzzWizzz Consultant App


*/

/* New API's required for BizzzWizzz App

API 1:   http://bizzzwizzz.com/index.php/Rest/signup

Params: name,mobile,email,panNo,pass,profileType

Response: success/failure.(return success and basic info similar to login api)

*/

/*
    1 1xx Informational.
    2 2xx Success.
    3 3xx Redirection.
    4 4xx Client Error.
    5 5xx Server Error.
*/
