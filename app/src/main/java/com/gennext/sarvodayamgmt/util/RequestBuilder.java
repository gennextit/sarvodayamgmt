package com.gennext.sarvodayamgmt.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */




public class RequestBuilder {
    public static RequestBody Default(String consultantId) {
        return new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .build();
    }
    public static List<KeyValuePair> DefaultUser(String userId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("userId",userId));
        return params;
    }

    public static RequestBody getRequestBody(List<KeyValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (KeyValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
//        RequestBody formBody = builder.build();
        return builder.build();
    }

    public static RequestBody uploadMultipleImages(String userId, List<File> file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            for (File model : file) {
                builder.addFormDataPart("image[]", model.getName(),
                        RequestBody.create(MediaType.parse("text/plain"), model));
            }
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody uploadSingleImages(String userId, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            builder.addFormDataPart("image", file.getName(),
                    RequestBody.create(MediaType.parse("text/plain"), file));
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody FeedbackDetail(String consultantId, String feedback) {
        return new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("feedback", feedback)
                .build();
    }

    public static RequestBody ErrorReport(String report) {
        return new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
    }


    public static RequestBody LoginBody(String username, String password) {
        return new FormBody.Builder()
                .addEncoded("userId", username)
                .addEncoded("password", password)
                .build();
    }

    public static RequestBody ByDoctorDate(String doctorId, String appDate) {
        return new FormBody.Builder()
                .addEncoded("DoctorId", doctorId)
                .addEncoded("AppDate", appDate)
                .build();
    }

    public static RequestBody employeeLogin(String username, String password, String companyId) {
        return new FormBody.Builder()
                .addEncoded("username", username)
                .addEncoded("password", password)
                .addEncoded("companyId", companyId)
                .build();
    }

}
