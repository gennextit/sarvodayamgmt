package com.gennext.sarvodayamgmt.util;

/**
 * Created by Admin on 11/24/2017.
 */

public class DBHelperConst {

    // Common column names
    protected static final String KEY_ID = "id";
    protected static final String KEY_CREATED_AT = "created_at";
    protected static final String KEY_USER_ID = "userId";
    protected static final String KEY_EMAIL = "email";
    protected static final String KEY_PASSWORD = "password";

}
