package com.gennext.sarvodayamgmt.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.setting.Const;

import static com.gennext.sarvodayamgmt.util.AppUser.COMMON;

/**
 * Created by Admin on 7/4/2017.
 */

public class AppConfig {
    private static final String TAG_STATUS_BAR =COMMON +"appStatusBar";
    private static final String TAG_ACTION_BAR = COMMON +"appActionBar";
    private static final String TAG_THEME = COMMON +"tag_theme";
    private static final String TAG_SIDEMENU_ICON = COMMON +"tag_sidemenu_icon";
    private static final String TAG_SIDEMENU_TEXT = COMMON +"color_sidemenu_text";
    private static final String TAG_SIDEMENU_DIVIDER =COMMON + "color_sidemenu_divider";
    private static final String TAG_DIALOG_DETAIL = COMMON +"dialog_detail";
    private static final String TAG_DIALOG_BTN_ACTION = COMMON +"dialog_btn_action";
    private static final String TAG_DIALOG_BTN_NORMAL =COMMON + "dialog_btn_normal";

    private static final String TAG_DIMEN_MENU_PROFILE = COMMON +"dimen_menu_profile";
    private static final String TAG_DIMEN_MENU_LIST = COMMON +"dimen_menu_list";
    private static final String TAG_DIMEN_DIALOG = COMMON +"dimen_dialog";
    public static final String FOLDER_XLS = "BackupInExel";


    // update both set and get methods
    public static void setAppDimens(Context context, int type, int textSize) {
        switch (type) {
            case Const.DIMEN_MENU_PROFILE:
                SavePref(context, TAG_DIMEN_MENU_PROFILE, textSize);
                break;
            case Const.DIMEN_MENU_LIST:
                SavePref(context, TAG_DIMEN_MENU_LIST, textSize);
                break;
            case Const.DIMEN_DIALOG:
                SavePref(context, TAG_DIMEN_DIALOG, textSize);
                break;
        }
    }

    // update both set and get methods
    // also update color parameter
    public static int getAppDimens(Context context, int type) {
        int dimen = getDimen(context, R.dimen.text_size_small);
        switch (type) {
            case Const.DIMEN_MENU_PROFILE:
                return LoadPref(context, TAG_DIMEN_MENU_PROFILE, dimen);
            case Const.DIMEN_MENU_LIST:
                return LoadPref(context, TAG_DIMEN_MENU_LIST, dimen);
            case Const.DIMEN_DIALOG:
                return LoadPref(context, TAG_DIMEN_DIALOG, dimen);
            default:
                return 0;
        }
    }


    // update both set and get methods
    public static void setAppColor(Context context, int type, int updateColor) {
        switch (type) {
            case Const.COLOR_STATUS_BAR:
                SavePref(context, TAG_STATUS_BAR, updateColor);
                break;
            case Const.COLOR_ACTION_BAR:
                SavePref(context, TAG_ACTION_BAR, updateColor);
                break;
            case Const.COLOR_SIDEMENU_ICON:
                SavePref(context, TAG_SIDEMENU_ICON, updateColor);
                break;
            case Const.COLOR_SIDEMENU_TEXT:
                SavePref(context, TAG_SIDEMENU_TEXT, updateColor);
                break;
            case Const.COLOR_SIDEMENU_DIVIDER:
                SavePref(context, TAG_SIDEMENU_DIVIDER, updateColor);
                break;
            case Const.COLOR_DIALOG_DETAIL:
                SavePref(context, TAG_DIALOG_DETAIL, updateColor);
                break;
            case Const.COLOR_DIALOG_BTN_ACTION:
                SavePref(context, TAG_DIALOG_BTN_ACTION, updateColor);
                break;
            case Const.COLOR_DIALOG_BTN_NORMAL:
                SavePref(context, TAG_DIALOG_BTN_NORMAL, updateColor);
                break;

        }
    }


    // update both set and get methods
    // also update color parameter
    public static int getAppColor(Context context, int type) {
        switch (type) {
            case Const.COLOR_STATUS_BAR:
                return LoadPref(context, TAG_STATUS_BAR, getCol(context, R.color.colorPrimaryDark));
            case Const.COLOR_ACTION_BAR:
                return LoadPref(context, TAG_ACTION_BAR, getCol(context, R.color.colorPrimary));
            case Const.COLOR_SIDEMENU_ICON:
                return LoadPref(context, TAG_SIDEMENU_ICON, getCol(context, R.color.colorPrimary));
            case Const.COLOR_SIDEMENU_TEXT:
                return LoadPref(context, TAG_SIDEMENU_TEXT, getCol(context, R.color.textAppColor));
            case Const.COLOR_SIDEMENU_DIVIDER:
                return LoadPref(context, TAG_SIDEMENU_DIVIDER, getCol(context, R.color.dividerColor));
            case Const.COLOR_DIALOG_DETAIL:
                return LoadPref(context, TAG_DIALOG_DETAIL, getCol(context, R.color.textAppColor));
            case Const.COLOR_DIALOG_BTN_ACTION:
                return LoadPref(context, TAG_DIALOG_BTN_ACTION, getCol(context, R.color.buttonActiveColor));
            case Const.COLOR_DIALOG_BTN_NORMAL:
                return LoadPref(context, TAG_DIALOG_BTN_NORMAL, getCol(context, R.color.buttonDisableColor));
            default:
                return 0;
        }
    }

    public static void setAppTheme(Context context, int type) {
        switch (type) {
            case Const.THEME_DARK:
                SavePref(context, TAG_THEME, Const.THEME_DARK);
                break;
            case Const.THEME_LIGHT:
                SavePref(context, TAG_THEME, Const.THEME_LIGHT);
                break;
        }
    }

    public static int getAppTheme(Context context) {
        return LoadPref(context, TAG_THEME, Const.THEME_LIGHT);
    }

    private static int LoadPref(Context context, String key, int defaultColor) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getInt(key, defaultColor);
        }
        return 0;
    }

    private static void SavePref(Context context, String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            editor.putInt(key, value);
            editor.apply();
        }
    }

    private static int getCol(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

//    public static boolean getFingerprintUsage(Context context) {
//        return Utility.LoadPrefBoolean(context,FINGER_PRINT,false);
//    }
//
//    public static void setFingerprintUsage(Context context, boolean value) {
//        Utility.SavePrefBoolean(context,FINGER_PRINT,value);
//    }


    private static int getDimen(Context context, int id) {
        float scaleRatio = context.getResources().getDisplayMetrics().density;

        float dimenPix = context.getResources().getDimension(id);

        float dimenOrginal = dimenPix/scaleRatio;
        return (int)dimenOrginal;
    }

}
