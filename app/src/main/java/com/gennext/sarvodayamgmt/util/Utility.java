package com.gennext.sarvodayamgmt.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import com.gennext.sarvodayamgmt.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

//import com.github.mikephil.charting.utils.ColorTemplate;

/**
 * Created by Abhijit on 23-Dec-16.
 */
public class Utility {

    public static final int[] SIDE_LINE_COLORS = {
            Color.rgb(160, 212, 104), Color.rgb(255, 206, 85), Color.rgb(172, 146, 235),
            Color.rgb(251, 110, 82), Color.rgb(143, 206, 242), Color.rgb(189, 189, 189),
            Color.rgb(255, 206, 85), Color.rgb(172, 146, 235), Color.rgb(251, 110, 82),
            Color.rgb(143, 206, 242), Color.rgb(255, 152, 0), Color.rgb(77, 208, 225),
            Color.rgb(144, 164, 174), Color.rgb(161, 136, 127), Color.rgb(79, 195, 247),
            Color.rgb(186, 104, 200), Color.rgb(205, 220, 57), Color.rgb(140, 192, 91),
            Color.rgb(246, 187, 67), Color.rgb(150, 123, 220), Color.rgb(223, 87, 62),
            Color.rgb(93, 156, 236)
    };



    private static int arrayIndexFindingByMatchString(List<String> list, String userSTring) {
        return Arrays.asList(list).indexOf(userSTring);
    }

    public String encodeUrl(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

//    private SpannableString generateCenterSpannableText() {
//        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
//        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
//        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
//        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
//        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
//        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
//        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
//        return s;
//    }

    public static SpannableString generateSpannableTitle(Context context, String title, String noramalText) {
        return generateSpannableTitle(title, noramalText, ContextCompat.getColor(context, R.color.colorGreen), false);
    }

    public static SpannableString generateSpannableTitle(Context context, String title, String noramalText, Boolean isTitleBold) {
        return generateSpannableTitle(title, noramalText, ContextCompat.getColor(context, R.color.colorGreen), isTitleBold);
    }

    public static SpannableString generateSpannableTitle(String title, String noramalText, int titleColor, Boolean isTitleBold) {
        SpannableString s = new SpannableString(title + " " + noramalText);
        if (isTitleBold) {
            s.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
        }
        s.setSpan(new ForegroundColorSpan(titleColor), 0, title.length(), 0);
        return s;
    }

    public static SpannableString generateBoldTitle(String title, String noramalText) {
        SpannableString s = new SpannableString(title + " " + noramalText);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
        return s;
    }

    public static String LoadPref(Context context, String key) {
        return LoadPref(context, key, "");
    }

    public static String LoadPref(Context context, String key, String defaultValue) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, defaultValue);
            return data;

        }
        return "";
    }

    public static Boolean LoadPrefBoolean(Context context, String key) {
        return LoadPrefBoolean(context, key, false);
    }

    public static Boolean LoadPrefBoolean(Context context, String key, Boolean defaultValue) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getBoolean(key, defaultValue);

        }
        return false;
    }

    public static void SavePref(Context context, String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            editor.putInt(key, value);
            editor.apply();
        }
    }

    public static void SavePrefBoolean(Context context, String key, Boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            editor.putBoolean(key, value);
            editor.apply();
        }
    }

    public static int LoadPrefInteger(Context context, String key) {
        return LoadPrefInteger(context, key, 0);
    }

    public static int LoadPrefInteger(Context context, String key, int defaultValue) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            int data = sharedPreferences.getInt(key, defaultValue);
            return data;
        }
        return 0;
    }

    public static void SavePref(Context context, String key, String name) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null && name != null) {
            editor.putString(key, name);
            editor.apply();
        }
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public static void getWhatsAppMsg(Activity activity) {
        Intent waIntent = new Intent(Intent.ACTION_SEND);
        waIntent.setType("text/plain");
        String text = "Sorry For Interruption,I'm Just Trying Something";
        waIntent.setPackage("com.whatsapp");
        if (waIntent != null) {
            waIntent.putExtra(Intent.EXTRA_TEXT, text);//
            activity.startActivity(Intent.createChooser(waIntent, "Share with"));
        }
    }

    public static void openWhatsAppMsgToSpecificPerson(Activity activity, String number) {
//        Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:<whatsappnumber>");
//        intent.setpackage("com.whatsapp");
//        startActivity(intent);
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");

        activity.startActivity(sendIntent);
    }

    public static void sendWhatsAppMsgToSpecificPerson(Activity activity, String number, String message) {
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);

        activity.startActivity(sendIntent);
    }


    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }


    public static double parseDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }


    public static String decimalFormat(float value) {
        return decimalFormat(value, "#");
    }

    public static String decimalFormat(float value, String type) {
        switch (type) {
            case "#":
                return new DecimalFormat("#").format(value);
            case "#.#":
                return new DecimalFormat("#.#").format(value);
            case "#.##":
                return new DecimalFormat("#.##").format(value);
            case "#.###":
                return new DecimalFormat("#.###").format(value);
            default:
                return new DecimalFormat("#").format(value);
        }
    }

}
