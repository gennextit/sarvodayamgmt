package com.gennext.sarvodayamgmt.search;

/**
 * Created by Admin on 5/22/2017.
 */

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.transition.Fade;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.DetailsTransition;


public class ShortSetting extends CompactFragment {


    public static final String SORT_DATE = "sortByDate", SORT_DOCTOR="sortByDoctor";
    private Sort listener;
    private LinearLayout llApply;
    private RadioGroup rgSort;
    private String sltSort;

    public interface Sort{
        void onSortBy(String sortBy);
    }

    public static Fragment newInstance(Sort listener) {
        ShortSetting fragment=new ShortSetting();
        fragment.listener=listener;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

    public static Fragment newInstance(String sortBy, Sort listener) {
        ShortSetting fragment=new ShortSetting();
        fragment.listener=listener;
        fragment.sltSort=sortBy;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.setting_sort, container, false);
        InitUI(v);
        return v;
    }
    private void InitUI(View v) {
        rgSort = (RadioGroup) v.findViewById(R.id.rg_sort);
        RadioButton rbDate = (RadioButton) v.findViewById(R.id.rb_sort_date);
        RadioButton rbDoctor = (RadioButton) v.findViewById(R.id.rb_sort_doctor);
        llApply = (LinearLayout) v.findViewById(R.id.ll_button_apply);
        llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSortBy(sltSort);
                getFragmentManager().popBackStack();
            }
        });
        if(sltSort==null || sltSort.equals(SORT_DATE)) {
            sltSort = SORT_DATE;
            rbDate.setChecked(true);
        }else{
            rbDoctor.setChecked(true);
        }
        rgSort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if(checkedId == R.id.rb_sort_date) {
                    sltSort = SORT_DATE;
                } else if(checkedId == R.id.rb_sort_doctor) {
                    sltSort = SORT_DOCTOR;
                }
            }
        });
    }


}
