package com.gennext.sarvodayamgmt.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.MainActivity;
import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.AppointmentModel;
import com.gennext.sarvodayamgmt.model.AppointmentModel;
import com.gennext.sarvodayamgmt.model.DoctorSelectorAdapter;
import com.gennext.sarvodayamgmt.pannel.DoctorSelector;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.JsonParser;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class Dashboard extends CompactFragment implements View.OnClickListener{
    private ImageView ivProfile;
    private TextView tvProfileName;
    private AssignTask assignTask;
    private TextView tvTotal,tvAmount,tvUnpaidCount;
    private LinearLayout llReport;
    private ProgressBar pbProgress;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Dashboard fragment=new Dashboard();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_dashboard,container,false);
        ((MainActivity)getActivity()).updateTitle("Dashboard");
        initUi(v);
        executeTask();
        return v;
    }

    private void initUi(View v) {
        pbProgress = (ProgressBar) v.findViewById(R.id.pb_progress);
        tvTotal = (TextView) v.findViewById(R.id.tv_total_appointments);
        tvAmount = (TextView) v.findViewById(R.id.tv_total_amount);
        tvUnpaidCount = (TextView) v.findViewById(R.id.tv_total_unpaid_count);
        llReport = (LinearLayout) v.findViewById(R.id.ll_report_dashboard);
        LinearLayout llmenuOption4 = (LinearLayout) v.findViewById(R.id.ll_menu_option_4);
        LinearLayout llmenuOption3 = (LinearLayout) v.findViewById(R.id.ll_menu_option_3);
        LinearLayout llmenuOption2 = (LinearLayout) v.findViewById(R.id.ll_menu_option_2);
        LinearLayout llmenuOption1 = (LinearLayout) v.findViewById(R.id.ll_menu_option_1);
        tvProfileName = (TextView) v.findViewById(R.id.tv_profile_name);
        ivProfile = (ImageView) v.findViewById(R.id.iv_profile);

        llmenuOption4.setOnClickListener(this);
        llmenuOption3.setOnClickListener(this);
        llmenuOption2.setOnClickListener(this);
        llmenuOption1.setOnClickListener(this);
        llmenuOption1.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
    }
    // update from main activity
    public void updateUi() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_menu_option_1:
                addFragment(AppByDoctor.newInstance(),"byDoctor");
                break;
            case R.id.ll_menu_option_2:
                addFragment(AppByDepartment.newInstance(),"appByDepartment");
                break;
            case R.id.ll_menu_option_3:
                showToast(getContext(), "Update Later");
                break;
            case R.id.ll_menu_option_4:
                showToast(getContext(), "Update Later");
                break;
            case R.id.iv_profile:
                showToast(getContext(), "Update Later");
                break;
        }
    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_ALL_TODAY_APPOINTMENTS);
    }

    private class AssignTask extends AsyncTask<String, Void, AppointmentModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppAnimation.fadeIn(pbProgress);
        }

        @Override
        protected AppointmentModel doInBackground(String... urls) {
            String response = null;
            response = ApiCall.GET(urls[0]);
            return JsonParser.getAllTodayAppointments(response);
        }


        @Override
        protected void onPostExecute(AppointmentModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pbProgress.setVisibility(View.GONE);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    AppAnimation.setViewAnimation(llReport,AppAnimation.ZOOM_IN);
                    tvTotal.setText("All Appointments : "+String.valueOf(result.getAppList().size()));
                    tvAmount.setText("Amount : "+result.getAmount());
                    tvUnpaidCount.setText("Unpaid : "+result.getPaymentStatus());
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    AppAnimation.setViewAnimation(llReport,AppAnimation.ZOOM_IN);
                    tvTotal.setText("Today Appointments : "+String.valueOf(result.getAppList().size()));
                    tvAmount.setText("Amount : "+result.getAmount());
                    tvUnpaidCount.setText("Unpaid : "+result.getPaymentStatus());
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {
                            dialog.dismiss();
                        }
                    })
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }
}
