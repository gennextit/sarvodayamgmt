package com.gennext.sarvodayamgmt.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.CircularsAdapter;
import com.gennext.sarvodayamgmt.model.CircularsModel;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.AppUser;
import com.gennext.sarvodayamgmt.util.RequestBuilder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class Circulars extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<CircularsModel> cList;
    private CircularsAdapter adapter;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public static Fragment newInstance() {
        Circulars fragment=new Circulars();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_circulars,container,false);
        initToolBar(getActivity(),v,"Circulars");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });

        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN_COMPANY);
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private class AssignTask extends AsyncTask<String, Void, CircularsModel> {
        private Context context;
        HashMap<String, String> data;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CircularsModel doInBackground(String... urls) {
            String response = null;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(userId, ""));
            return getCircularsList(getActivity());
        }


        @Override
        protected void onPostExecute(CircularsModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    cList = result.getList();
                    adapter = new CircularsAdapter(getActivity(), cList);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }


    }


    // Const params are must be same
    private CircularsModel getCircularsList(Context context) {
        CircularsModel model = new CircularsModel();
        model.setOutput(Const.SUCCESS);
        model.setList(new ArrayList<CircularsModel>());
        model.getList().add(setItem("12-12-2016","Price Escalation","Central Govt.","By Admin"));
        model.getList().add(setItem("15-02-2017","Export updates","State Govt.","Admin"));
        model.getList().add(setItem("22-02-2017","Discount Schemes","UPSMA","Admin"));
        return model;
    }

    private CircularsModel setItem(String date, String title, String issuer, String uploadedBy) {
        CircularsModel model=new CircularsModel();
        model.setDate(date);
        model.setTitle(title);
        model.setIssuer(issuer);
        model.setUploadedBy(uploadedBy);
        return model;
    }
}
