package com.gennext.sarvodayamgmt.user;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.DatePickerDialog;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.AppByDepartmentExpandAdapter;
import com.gennext.sarvodayamgmt.model.AppointmentModel;
import com.gennext.sarvodayamgmt.model.DoctorModel;
import com.gennext.sarvodayamgmt.model.EXELMaker;
import com.gennext.sarvodayamgmt.pannel.DepartmentSelector;
import com.gennext.sarvodayamgmt.search.ShortSetting;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.DateTimeUtility;
import com.gennext.sarvodayamgmt.util.FileUtils;
import com.gennext.sarvodayamgmt.util.JsonParser;
import com.gennext.sarvodayamgmt.util.RequestBuilder;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Admin on 7/4/2017.
 */

public class AppByDepartment extends CompactFragment implements DatePickerDialog.DateSelectFlagListener, DepartmentSelector.SelectListener {
    private static final int WEEK_GAP = 7;
    //    private RecyclerView lvMain;
    private ArrayList<AppointmentModel> cList;
    private AppByDepartmentExpandAdapter adapter;
    private AssignTask assignTask;
    //    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ExpandableListView lv;
    private TextView tvTotalCount, tvApply;
    private String mStartDate, mEndDate;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private LinearLayout llStartDate, llEndDate, llApply, llSelectDepartment;
    private TextView tvStartDate, tvEndDate, tvDepartment;
    private DoctorModel mDepartmentList;
    private ProgressBar progressBar;
    private DoctorModel mSelectedDepartment;
    private EXELMaker exelMaker;
    private int mStartDateStamp,mEndDateStamp;
    private String mSortBy;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        AppByDepartment fragment = new AppByDepartment();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_by_department, container, false);
        Toolbar toolbar = initToolBar(getActivity(), v, "Appointment By Department");
        setMenuOptions(toolbar);
        exelMaker = EXELMaker.newInstance(getActivity());
        initDate();
        initUi(v);
        return v;
    }



    private void initDate() {
        //Calendar set to the current date
        Calendar calendar = Calendar.getInstance();
        eYear = calendar.get(Calendar.YEAR);
        eMonth = calendar.get(Calendar.MONTH);
        eDay = calendar.get(Calendar.DAY_OF_MONTH);
        mEndDateStamp = DateTimeUtility.convertDateStamp(eYear,eMonth,eDay);
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);


        //rollback 90 days
        calendar.add(Calendar.DAY_OF_YEAR, -WEEK_GAP);
        //now the date is 90 days back
        sYear = calendar.get(Calendar.YEAR);
        sMonth = calendar.get(Calendar.MONTH);
        sDay = calendar.get(Calendar.DAY_OF_MONTH);
        mStartDateStamp = DateTimeUtility.convertDateStamp(sYear,sMonth,sDay);
        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);
    }

    private void initUi(View v) {

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        llStartDate = (LinearLayout) v.findViewById(R.id.ll_button_start_date);
        llEndDate = (LinearLayout) v.findViewById(R.id.ll_button_end_date);
        llSelectDepartment = (LinearLayout) v.findViewById(R.id.ll_button_select_doctor);
        llApply = (LinearLayout) v.findViewById(R.id.ll_button_apply);
        tvStartDate = (TextView) v.findViewById(R.id.tv_start_date);
        tvEndDate = (TextView) v.findViewById(R.id.tv_end_date);
        tvDepartment = (TextView) v.findViewById(R.id.tv_doctor_name);
        tvTotalCount = (TextView) v.findViewById(R.id.tv_total_count);
        tvApply = (TextView) v.findViewById(R.id.tv_apply);
//        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
//        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        lvMain.setLayoutManager(horizontalManager);
//        lvMain.setItemAnimator(new DefaultItemAnimator());
        lv = (ExpandableListView) v.findViewById(R.id.expandableListView1);
        lv.setFocusable(false);
        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
//                if (rwaList != null) {
//                    String name=rwaList.get(groupPosition).getChildList().get(childPosition).getName();
//                    String image=rwaList.get(groupPosition).getChildList().get(childPosition).getImage();
//                    String designation=rwaList.get(groupPosition).getChildList().get(childPosition).getDesignation();
//                    String detail=rwaList.get(groupPosition).getChildList().get(childPosition).getDetail();
//                    String availableFrom=rwaList.get(groupPosition).getChildList().get(childPosition).getAvailableFrom();
//
//                    ImpContactDetail impContactDetail=new ImpContactDetail();
//                    impContactDetail.setData(name,image,designation,detail,availableFrom);
//                    addFragment(impContactDetail,android.R.id.content,"impContactDetail");
//                }
                return false;
            }
        });
//        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                executeTask();
//            }
//        });

        llStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(AppByDepartment.this, false, DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });
        llEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(AppByDepartment.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });

        llApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });

        llSelectDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DepartmentSelector.newInstance(AppByDepartment.this, mDepartmentList).show(getFragmentManager(), "departmentSelector");
            }
        });

        tvEndDate.setText(mEndDate);
        tvStartDate.setText(mStartDate);

//        executeTask();
    }

    private void executeTask() {
        if (mSelectedDepartment==null || TextUtils.isEmpty(mSelectedDepartment.getDepartment())) {
            showToast(getContext(), "Please select Doctor");
            return;
        }
        if(mStartDateStamp>mEndDateStamp){
            showToast(getContext(), "Start date should be less than end date");
            return;
        }
//        mSwipeRefreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                mSwipeRefreshLayout.setEnabled(true);
//                mSwipeRefreshLayout.setRefreshing(true);
//            }
//        });
        assignTask = new AssignTask(getActivity(), mStartDate, mEndDate, mSelectedDepartment);
        assignTask.execute(AppSettings.VIEW_APPOINTMENT_BY_DOCTOR);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(assignTask!=null){
            assignTask.onDetach();
            assignTask.cancel(true);
        }
    }

    private void hideProgressBar() {
//        if (mSwipeRefreshLayout != null) {
//            mSwipeRefreshLayout.setEnabled(false);
//            mSwipeRefreshLayout.setRefreshing(false);
//        }
    }

    @Override
    public void onDepartmentSelect(DialogFragment dialog, DoctorModel mDepartmentList, DoctorModel selectedDepartment) {
        this.mDepartmentList = mDepartmentList;
        this.mSelectedDepartment = selectedDepartment;
        tvDepartment.setText(selectedDepartment.getDepartment());
    }



    private class AssignTask extends AsyncTask<String, Integer, AppointmentModel> {
        private final DoctorModel mDepartment;
        private Context context;
        private String startDate, endDate;
        private int count;
        private int dateSize;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String mStartDate, String mEndDate, DoctorModel mDepartment) {
            this.context = context;
            startDate = mStartDate;
            endDate = mEndDate;
            this.mDepartment = mDepartment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            count = 1;
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
            disableButton();
        }

        @Override
        protected AppointmentModel doInBackground(String... urls) {
            if(mSortBy==null || mSortBy.equals(ShortSetting.SORT_DATE)){
                return sortByDateAppointments(urls);
            }else{
                return sortByDoctorAppointments(urls);
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(AppointmentModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                progressBar.setVisibility(View.GONE);
                hideProgressBar();
                enableButton();
                if (result.getDateList().size() > 0) {
                    cList = result.getDateList();
                    tvTotalCount.setVisibility(View.VISIBLE);
                    tvTotalCount.setText("Total Appointment is " + String.valueOf(getTotalAppointments(cList)));
                    adapter = new AppByDepartmentExpandAdapter(getActivity(), cList, lv, mSortBy,AppByDepartment.this);
                    lv.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }

        private AppointmentModel sortByDateAppointments(String... urls) {
            String response = null;
            AppointmentModel result = new AppointmentModel();
            result.setDateList(new ArrayList<AppointmentModel>());
            ArrayList<Date> dateList = DateTimeUtility.getDateFromRange(startDate, endDate);
            if (!mDepartment.getDepartment().equalsIgnoreCase(DepartmentSelector.ALL_DEPARTMENT)) {
                dateSize = dateList.size();
            }else{
                dateSize = dateList.size() * mDepartmentList.getDoctorList().size();
            }
            for (Date date : dateList) {
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date);
                AppointmentModel dateModel = new AppointmentModel();
                dateModel.setInputDate(currentDate);
                dateModel.setAllDoctors(new ArrayList<String>());
                dateModel.setAppList(new ArrayList<AppointmentModel>());

                if (!mDepartment.getDepartment().equalsIgnoreCase(DepartmentSelector.ALL_DEPARTMENT)) {
                    for (DoctorModel doctor : mDepartment.getDoctorList()) {
                        dateModel.getAllDoctors().add(doctor.getDoctorName());
                        response = ApiCall.POST(urls[0], RequestBuilder.ByDoctorDate(doctor.getDoctorId(), currentDate));
                        dateModel = JsonParser.parseLoadAppointmentByDoctor(dateModel, response, doctor);
                        publishProgress((int) ((count / (float) dateSize) * 100));
                        count++;
                        if(context==null){
                            break;
                        }
                    }
                    dateModel.setTotal(String.valueOf(dateModel.getAppList().size()));
                    result.getDateList().add(dateModel);
                } else {
                    for (DoctorModel doctor : mDepartmentList.getDoctorList()) {
                        response = ApiCall.POST(urls[0], RequestBuilder.ByDoctorDate(doctor.getDoctorId(), currentDate));
                        dateModel = JsonParser.parseLoadAppointmentByDoctor(dateModel, response, doctor);
                        publishProgress((int) ((count / (float) dateSize) * 100));
                        count++;
                        if(context==null){
                            break;
                        }
                    }
                    dateModel.getAllDoctors().add("All Doctors");
                    dateModel.setTotal(String.valueOf(dateModel.getAppList().size()));
                    result.getDateList().add(dateModel);
                }
                if(context==null){
                    break;
                }
            }
            return result;
        }

        private AppointmentModel sortByDoctorAppointments(String... urls) {
            String response = null;
            AppointmentModel result = new AppointmentModel();
            result.setDateList(new ArrayList<AppointmentModel>());
            ArrayList<Date> dateList = DateTimeUtility.getDateFromRange(startDate, endDate);
            if (!mDepartment.getDepartment().equalsIgnoreCase(DepartmentSelector.ALL_DEPARTMENT)) {
                dateSize = dateList.size();
            }else{
                dateSize = dateList.size() * mDepartmentList.getDoctorList().size();
            }

            if (!mDepartment.getDepartment().equalsIgnoreCase(DepartmentSelector.ALL_DEPARTMENT)) {
                for (DoctorModel doctor : mDepartment.getDoctorList()) {
                    AppointmentModel dateModel = new AppointmentModel();
                    dateModel.setAppList(new ArrayList<AppointmentModel>());
                    for (Date date : dateList) {
                        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date);
                        response = ApiCall.POST(urls[0], RequestBuilder.ByDoctorDate(doctor.getDoctorId(), currentDate));
                        dateModel = JsonParser.parseLoadAppointmentByDoctor(dateModel, response, doctor);
                        publishProgress((int) ((count / (float) dateSize) * 100));
                        count++;
                        if(context==null){
                            break;
                        }
                    }
                    dateModel.setDepartment(doctor.getDoctorName());
                    dateModel.setTotal(String.valueOf(dateModel.getAppList().size()));
                    result.getDateList().add(dateModel);
                    if(context==null){
                        break;
                    }
                }

            } else {
                for (DoctorModel department : mDepartmentList.getList()) {
                    AppointmentModel dateModel = new AppointmentModel();
                    dateModel.setAppList(new ArrayList<AppointmentModel>());
                    for (DoctorModel doctor : department.getDoctorList()) {
                        for (Date date : dateList) {
                            String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date);
                            response = ApiCall.POST(urls[0], RequestBuilder.ByDoctorDate(doctor.getDoctorId(), currentDate));
                            dateModel = JsonParser.parseLoadAppointmentByDoctor(dateModel, response, doctor);
                            publishProgress((int) ((count / (float) dateSize) * 100));
                            count++;
                            if(context==null){
                                break;
                            }
                        }
                        if(context==null){
                            break;
                        }
                    }
                    dateModel.setDepartment(department.getDepartment());
                    dateModel.setTotal(String.valueOf(dateModel.getAppList().size()));
                    result.getDateList().add(dateModel);
                }

            }
            return result;
        }


    }

    private void disableButton() {
        llApply.setClickable(false);
        tvApply.setText("Loading...");
    }

    private void enableButton() {
        llApply.setClickable(true);
        tvApply.setText("Apply");
    }

    private int getTotalAppointments(ArrayList<AppointmentModel> cList) {
        int count = 0;
        for (AppointmentModel model : cList) {
            count += model.getAppList().size();
        }
        return count;
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        mStartDateStamp = DateTimeUtility.convertDateStamp(sYear,sMonth,sDay);
        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);
        tvStartDate.setText(mStartDate);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mEndDateStamp = DateTimeUtility.convertDateStamp(eYear,eMonth,eDay);
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);
        tvEndDate.setText(mEndDate);
    }


    private void setMenuOptions(Toolbar toolbar) {
        toolbar.inflateMenu(R.menu.export_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                File mOutputFile;

                switch (item.getItemId()) {
                    case R.id.navigation_menu_0:
                        addFragment(ShortSetting.newInstance(mSortBy,new ShortSetting.Sort() {
                            @Override
                            public void onSortBy(String sortBy) {
                                mSortBy = sortBy;
                                executeTask();
                            }
                        }),"shortSetting");
//                        showToast(getContext(), "Update Later");
                        return false;

                    case R.id.navigation_menu_1:
                        if (cList == null) {
                            return false;
                        }
                        mOutputFile = exelMaker.exportInExel(cList);
                        if (mOutputFile != null) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri uri = FileUtils.getUriFromFile(getContext(), mOutputFile);
                            intent.setDataAndType(uri, "application/vnd.ms-excel");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getContext(), "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
                            }
                        }
                        return false;

                    case R.id.navigation_menu_2:
                        if (cList == null) {
                            return false;
                        }
                        mOutputFile = exelMaker.exportInExel(cList);
                        if (mOutputFile != null) {
                            Uri uri = FileUtils.getUriFromFile(getContext(), mOutputFile);
                            Intent in = new Intent(Intent.ACTION_SEND);
                            in.setDataAndType(uri, "application/vnd.ms-excel");
                            in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            in.putExtra(Intent.EXTRA_STREAM, uri);
                            try {
                                startActivity(Intent.createChooser(in, "Share With Fiends"));
                            } catch (Exception e) {
                                Toast.makeText(getContext(), "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        return false;

                    default:
                        break;
                }

                return false;
            }
        });
    }

}
