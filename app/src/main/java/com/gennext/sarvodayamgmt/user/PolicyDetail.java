package com.gennext.sarvodayamgmt.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.CircularsAdapter;
import com.gennext.sarvodayamgmt.model.CircularsModel;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.AppUser;
import com.gennext.sarvodayamgmt.util.RequestBuilder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class PolicyDetail extends CompactFragment {

    public static Fragment newInstance() {
        PolicyDetail fragment=new PolicyDetail();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_policy_details,container,false);
        initUi(v);
        return v;
    }

    private void initUi(View v) {

    }


}
