package com.gennext.sarvodayamgmt.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.model.DimensSettingAdapter;
import com.gennext.sarvodayamgmt.model.DimensSettingModel;
import com.gennext.sarvodayamgmt.util.AppConfig;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class DimensSetting extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<DimensSettingModel> cList;
    private DimensSettingAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.setting_dimens,container,false);
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());


        cList = getDimensSettingList(getActivity());
        adapter = new DimensSettingAdapter(getActivity(), cList,DimensSetting.this);
        lvMain.setAdapter(adapter);

    }
    // Const params are must be same
    private ArrayList<DimensSettingModel> getDimensSettingList(Context context) {
        ArrayList<DimensSettingModel> list=new ArrayList<>();
        list.add(setItem(context,Const.DIMEN_MENU_PROFILE,"Side menu profile"));
        list.add(setItem(context,Const.DIMEN_MENU_LIST,"Side Menu list"));
        list.add(setItem(context,Const.DIMEN_DIALOG,"Dialog box"));
        return list;
    }

    private DimensSettingModel setItem(Context context, int type, String title) {
        DimensSettingModel model=new DimensSettingModel();
        model.setType(type);
        model.setTitle(title);
        model.setTextSize(AppConfig.getAppDimens(context,type));
        return model;
    }
}
