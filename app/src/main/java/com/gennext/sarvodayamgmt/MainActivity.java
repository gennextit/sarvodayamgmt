package com.gennext.sarvodayamgmt;

import android.Manifest;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import com.gennext.sarvodayamgmt.user.Dashboard;
import com.gennext.sarvodayamgmt.util.FileUtils;
import com.gennext.sarvodayamgmt.util.L;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

public class MainActivity extends NavDrawer implements BottomNavigationView.OnNavigationItemSelectedListener{

    private Toolbar mToolbar;
    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //startActivity(new Intent(getActivity(),NewLeadActivity.class));
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(getApplicationContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar=setToolBar(getSt(R.string.app_name));
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setAboutDetail();
        setSupportActionBar(mToolbar);
        SetDrawer(this,mToolbar);
        initUi();
        Uri path = FileUtils.getUriToDrawable(this, R.drawable.ic_gen_logo_128x);
        L.m(path.toString());

    }


    private void checkPermission() {
        new TedPermission(getApplicationContext())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.READ_CALL_LOG, Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS)
                .check();
    }




    private void initUi() {
        replaceFragmentWithoutBackstack(Dashboard.newInstance(), R.id.container_main, "dashboard");
    }

    public void updateTitle(String title) {
        mToolbar.setTitle(title);
    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

        if(backStackEntryCount==1){
            updateTitle("Dashboard");
            Dashboard dashboard= (Dashboard) getSupportFragmentManager().findFragmentByTag("dashboard");
            if(dashboard!=null){
                dashboard.updateUi();
            }
        }
        if(backStackEntryCount > 0){
            getSupportFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }

}
