package com.gennext.sarvodayamgmt.pannel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.DepartmentSelectorAdapter;
import com.gennext.sarvodayamgmt.model.DoctorModel;
import com.gennext.sarvodayamgmt.model.DoctorSelectorAdapter;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.JsonParser;
import com.gennext.sarvodayamgmt.util.ProgressRounded;

import java.util.ArrayList;


public class DepartmentSelector extends BottomSheetDialogFragment implements ApiCallError.ErrorParamListener {
    public static final String ALL_DEPARTMENT = "All Departmsnts";
    private SelectListener mListener;
    private DepartmentSelectorAdapter adapter;
    private RecyclerView lvMain;
    //    private DBManager dbManager;
    private AssignTask assignTask;
    private ProgressRounded pBar;
    private DoctorModel mDoctorList;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void onSelectedItem(DoctorModel item) {
        if (item != null) {
            if (mListener != null) {
                mDoctorList.getList().remove(0);
                mListener.onDepartmentSelect(DepartmentSelector.this, mDoctorList, item);
            }
            dismiss();
        }
    }



    public interface SelectListener {
        void onDepartmentSelect(DialogFragment dialog, DoctorModel finalList, DoctorModel selectedDepartment);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static DepartmentSelector newInstance(SelectListener selectListener) {
        DepartmentSelector fragment = new DepartmentSelector();
        fragment.mListener = selectListener;
        return fragment;
    }

    public static DepartmentSelector newInstance(SelectListener selectListener, DoctorModel mDoctorList) {
        DepartmentSelector fragment = new DepartmentSelector();
        fragment.mListener = selectListener;
        fragment.mDoctorList = mDoctorList;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_company, container, false);
//        dbManager = DBManager.newIsntance(getActivity());
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_menu_action);
        final EditText etSearch = (EditText) v.findViewById(R.id.et_search);

        pBar = ProgressRounded.newInstance(getContext(), v);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().length() == 0) {
                    dismiss();
                } else {
                    etSearch.setText("");
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        lvMain = (RecyclerView) v.findViewById(R.id.rv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        if(mDoctorList!=null){
            addAllDepartments();
            adapter = new DepartmentSelectorAdapter(getActivity(), mDoctorList.getList(), DepartmentSelector.this);
            lvMain.setAdapter(adapter);
        }else {
            executeTask();
        }

        return v;
    }

    private void addAllDepartments() {
        if(!mDoctorList.getList().get(0).getDepartment().equals(DepartmentSelector.ALL_DEPARTMENT)) {
            DoctorModel item2 = new DoctorModel();
            item2.setDepartment(DepartmentSelector.ALL_DEPARTMENT);
            mDoctorList.getList().add(0, item2);
        }
    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOAD_DEPARTMENT);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {
        dismiss();
    }


    private class AssignTask extends AsyncTask<String, Void, DoctorModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();
        }

        @Override
        protected DoctorModel doInBackground(String... urls) {
            String response = null;
            response = ApiCall.GET(urls[0]);
            DoctorModel department = JsonParser.getDepartmentList(response);
            if(department.getOutput().equalsIgnoreCase(Const.SUCCESS)){
                response = ApiCall.GET(AppSettings.LOAD_DOCTOR);
                DoctorModel doctor = JsonParser.getDoctorList(response);
                if(doctor.getOutput().equalsIgnoreCase(Const.SUCCESS)) {
                    department.setDoctorList(doctor.getList());
                    return JsonParser.mergeDoctorAndDepartment(department,doctor);
                }else{
                    return doctor;
                }
            }
            else {
                return department;
            }
        }


        @Override
        protected void onPostExecute(DoctorModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    mDoctorList = result;
                    adapter = new DepartmentSelectorAdapter(getActivity(), mDoctorList.getList(), DepartmentSelector.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
//                    errorList.add(result.getOutputMsg());
//                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_empty_product,
//                            R.id.tv_message, errorList);
//                    lvMain.setAdapter(adapter);
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    String[] errorSoon = {};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, DepartmentSelector.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }

}
