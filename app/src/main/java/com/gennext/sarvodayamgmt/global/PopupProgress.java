package com.gennext.sarvodayamgmt.global;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;


/**
 * Created by Abhijit on 19-Dec-16.
 */

/**Usage Instruction
 *
    // for showing dialog
    AlertDialog dialog=PopupProgress.newInstance(context,msg).show();

    // for hiding dialog
    dialog.dismiss();
 */
public class PopupProgress {

    private Context context;
    private String message;


    public static PopupProgress newInstance(Context context) {
        PopupProgress cDialog=new PopupProgress();
        cDialog.context=context;
        return cDialog;
    }

    public static PopupProgress newInstance(Context context, String message) {
        PopupProgress cDialog=new PopupProgress();
        cDialog.context=context;
        cDialog.message=message;
        return cDialog;
    }

    public AlertDialog show() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        TextView tvMessage = (TextView) dialogView.findViewById(R.id.tv_message);
        if(message==null){
            tvMessage.setText(context.getString(R.string.dialog_message));
        }else {
            tvMessage.setText(message);
        }
        AlertDialog b = dialogBuilder.create();
        b.show();
        return b;
    }

}

