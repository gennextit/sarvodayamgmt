package com.gennext.sarvodayamgmt.global;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.util.ApiCallError;


public class AppWebView extends CompactFragment implements ApiCallError.ErrorListener {
    String BrowserSetting = "";
    Activity context;
    WebView webView;
    ProgressBar progressBar;
    FragmentManager mannager;
    String webUrl;
    int URLTYPE = 0;
    String browserName;
    public static int URL = 0, PDF = 1;
    private LinearLayout llprogressBar;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }

    public static AppWebView newInstance(String title, String url, int urlType) {
        AppWebView fragment = new AppWebView();
        fragment.browserName = title;
        fragment.webUrl = url;
        fragment.URLTYPE = urlType;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.app_web_view, container, false);
        mannager = getFragmentManager();
        this.mContext = context;
        setActionBarOption(getActivity(), v);
        initUi(v);
        return v;
    }


    private void initUi(View v) {
        // TODO Auto-generated method stub
        webView = (WebView) v.findViewById(R.id.webView_browser);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        llprogressBar = (LinearLayout) v.findViewById(R.id.ll_progress);

        webView.setInitialScale(0);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    llprogressBar.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                } else {
                    llprogressBar.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        // Javascript inabled on webview

        if (URLTYPE == URL) {
            startWebView(webUrl);
        } else {
            String Doc = "<iframe src='http://docs.google.com/gview?embedded=true&url=" + webUrl + "' width='100%' height='100%' style='border: none;'></iframe>";
            startWebView(Doc);
        }

    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        startWebView(webUrl);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    public class myWebclient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {

            // If you will not use this method url links are opeen in new brower
            // not in webview
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (URLTYPE == URL) {
                    view.loadUrl(url);
                } else {
                    view.loadData(url, "text/html", "UTF-8");
                }
                return true;
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (URLTYPE == URL) {
                    view.loadUrl(request.getUrl().toString());
                } else {
                    view.loadData(request.getUrl().toString(), "text/html", "UTF-8");
                }
                return true;
            }

            // Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (mContext != null) {
                    ApiCallError.newInstance(error.getDescription() + "\n" + error.getErrorCode(), AppWebView.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                if (mContext != null) {
                    ApiCallError.newInstance(description + "\n" + errorCode, AppWebView.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }

        });


        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        //webView.setScrollbarFadingEnabled(false);
        if (URLTYPE == URL) {
            webView.loadUrl(url);
        } else {
            webView.loadData(url, "text/html", "UTF-8");
        }
    }


    public void setActionBarOption(final Activity act, View v) {

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(browserName);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(act);
                        if (webView.canGoBack()) {
                            webView.goBack();
                        } else {
                            // Let the system handle the back button
                            mannager.popBackStack();
                        }
                    }
                }

        );
    }

    // Detect when the back button is pressed
    public void onBackPressed() {

        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
            mannager.popBackStack();
        }
    }

    public void AlertInternet() {

        new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Connected!").setMessage("Please Check Your Internet Connection.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }

                }).show();
        // Toast.makeText(getApplicationContext(),"No Internet Connection
        // found...",Toast.LENGTH_LONG).show();

    }

}


