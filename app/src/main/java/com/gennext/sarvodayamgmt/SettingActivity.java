package com.gennext.sarvodayamgmt;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;

/**
 * Created by Admin on 7/4/2017.
 */

public class SettingActivity extends BaseActivity{

    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        NestedScrollView nsv = (NestedScrollView) findViewById(R.id.nsv);
        mToolbar=initToolBar("Setting");
        nsv.scrollTo(0,0);
    }

    public void updateActivityTheme() {
    }

    public void updateActivitColor(int type) {
    }
}
