package com.gennext.sarvodayamgmt;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sarvodayamgmt.global.PopupProgress;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.FileUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public abstract class NavDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private ProgressDialog navPDialog;
    private ImageView ivProfileBackground;
    private TextView tvProfileName;

    protected void setAboutDetail() {
        Button about =(Button)findViewById(R.id.btn_about);
        Double testName = BuildConfig.testVersionName;
        if(testName!=0){
            about.setText(getString(R.string.copyright_gennext)+"\nVersion "+String.valueOf(testName));
        }else{
            about.setText(getString(R.string.copyright_gennext)+"\nVersion "+String.valueOf(BuildConfig.VERSION_NAME));
        }
    }


    protected void SetDrawer(final Activity act, Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ImageView ivProfile = (ImageView) findViewById(R.id.iv_profile);
        TextView tvName = (TextView) findViewById(R.id.tv_profile_name);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_item_1) {

        } else if (id == R.id.nav_item_2) {

        } else if (id == R.id.nav_item_3) {

        } else if (id == R.id.nav_item_4) {

        } else if (id == R.id.nav_item_5) {

        } else if (id == R.id.nav_item_6) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void shareClick(View view) {
        new AssignTask(NavDrawer.this).execute(R.drawable.ic_gen_logo_128x);
    }

    private class AssignTask extends AsyncTask<Integer,Void,Uri> {

        private final Context context;
        private AlertDialog dialog;

        public AssignTask(Context context) {
            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // for showing dialog
            dialog= PopupProgress.newInstance(context).show();
        }

        @Override
        protected Uri doInBackground(Integer... image) {
            return FileUtils.getUriToDrawable(context,image[0]);

        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            // for hiding dialog
            dialog.dismiss();
            if(uri!=null) {
                String strUri = AppSettings.SHARE_URL + "01";//mItem.getId()

                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("image/*");
                in.putExtra(Intent.EXTRA_STREAM, uri);
                in.putExtra(Intent.EXTRA_TEXT, getString(R.string.app_name) + "\n\n" + "Title" + "\n\nRead more click the link below.\n" + strUri);
                startActivity(Intent.createChooser(in, "Share With Fiends"));
            }else{
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

//    public Toolbar setToolBar(String title) {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle(title);
//        setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.mipmap.ic_menu);
//        return toolbar;
//    }

    public Toolbar setToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        return toolbar;
    }

    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }



    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }


    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    public void showPDialog(String msg) {
        navPDialog = new ProgressDialog(NavDrawer.this);
        navPDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // navPDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        navPDialog.setMessage(msg);
        navPDialog.setIndeterminate(false);
        navPDialog.setCancelable(false);
        navPDialog.show();
    }

    public void dismissPDialog() {
        if (navPDialog != null)
            navPDialog.dismiss();
    }


    public int getCol(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

    public String LoadPref(String key) {
        return LoadPref(NavDrawer.this, key);
    }

    public String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getString(key, "");
        } else {
            return "";
        }
    }

    public void SavePref(String key, String value) {
        SavePref(NavDrawer.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public String encodeUrl(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }


    public String getSt(int id) {

        return getResources().getString(id);
    }


    public void showToast(String txt) {
        // Inflate the Layout
        Toast toast = Toast.makeText(NavDrawer.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
        toast.show();
    }



    public void closePrevoiusScreen() {
        getSupportFragmentManager().popBackStack();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void replaceFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }


}