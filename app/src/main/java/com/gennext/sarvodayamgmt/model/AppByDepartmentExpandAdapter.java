package com.gennext.sarvodayamgmt.model;

/**
 * Created by Abhijit on 17-Sep-16.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.search.ShortSetting;
import com.gennext.sarvodayamgmt.user.AppByDepartment;

import java.util.ArrayList;

public class AppByDepartmentExpandAdapter extends BaseExpandableListAdapter {

    private final AppByDepartment parentRef;
    private final String sortBy;
    private Activity context;
    private ArrayList<AppointmentModel> groups;
    private int lastExpandedGroupPosition;
    ExpandableListView listView;
    private AlertDialog dialog = null;

    public AppByDepartmentExpandAdapter(Activity context, ArrayList<AppointmentModel> groups, ExpandableListView listView, String sortBy, AppByDepartment parentRef) {
        this.context = context;
        this.groups = groups;
        this.parentRef = parentRef;
        this.listView = listView;
        this.sortBy = sortBy;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<AppointmentModel> chList = groups.get(groupPosition).getAppList();
        return chList.get(childPosition);
    }

    class ChildHolder {
        private LinearLayout llSlot;
        private TextView tvAppId,tvAppDate, tvAppTime;

        public ChildHolder(View v) {
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvAppId = (TextView) v.findViewById(R.id.tv_field_1);
            tvAppDate = (TextView) v.findViewById(R.id.tv_field_2);
            tvAppTime = (TextView) v.findViewById(R.id.tv_field_3);

        }
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        //collapse the old expanded group, if not the same
        //as new group to expand
//        if (groupPosition != lastExpandedGroupPosition) {
//            listView.collapseGroup(lastExpandedGroupPosition);
//        }
//
        super.onGroupExpanded(groupPosition);
//        lastExpandedGroupPosition = groupPosition;
//        setSelectGroup(groupPosition);
    }

    private void setSelectGroup(int groupPosition) {
        listView.setSelectedGroup(groupPosition);
        listView.expandGroup(groupPosition); //Expanding the group selected, now the list has 1 more element
        listView.smoothScrollToPosition(groupPosition-1);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final AppointmentModel child = (AppointmentModel) getChild(groupPosition, childPosition);
        View v = convertView;
        ChildHolder childHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_app_by_doctor, parent, false);
            childHolder = new ChildHolder(v);
            v.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) v.getTag();
        }
        childHolder.tvAppId.setText(child.getAppId());
        childHolder.tvAppDate.setText(child.getAppDate());
        childHolder.tvAppTime.setText(child.getAppTime());

        childHolder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                parentRef.showCallAlertBox(context, child.getName(), mobile);
            }
        });
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<AppointmentModel> chList = groups.get(groupPosition).getAppList();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    class GroupHolder {
        private TextView tvTotal, tvDate, tvMessage;
        private ImageView ivGroup;

        public GroupHolder(View v) {
            tvDate = (TextView) v.findViewById(R.id.group_date);
            tvTotal = (TextView) v.findViewById(R.id.group_name);
            tvMessage = (TextView) v.findViewById(R.id.group_message);
            ivGroup = (ImageView) v.findViewById(R.id.iv_group_icon);
        }
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        AppointmentModel group = (AppointmentModel) getGroup(groupPosition);
        View v = convertView;
        GroupHolder groupHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.group_item, parent, false);
            groupHolder = new GroupHolder(v);
            v.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) v.getTag();
        }

        if (isExpanded) {
            lastExpandedGroupPosition = groupPosition;
//            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_up);
        } else {
//            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_down);
        }
        int rotationAngle = isExpanded ? 180 : 0;  //toggle
        groupHolder.ivGroup.animate().rotation(rotationAngle).setDuration(50).start();
        groupHolder.tvTotal.setText(group.getTotal());
        if(group.getTotal().equalsIgnoreCase("0")) {
            groupHolder.tvMessage.setText("Message : " + group.getOutputMsg());
            groupHolder.tvMessage.setVisibility(View.VISIBLE);
        }else{
            groupHolder.tvMessage.setVisibility(View.GONE);
        }
        if(sortBy == null || sortBy.equals(ShortSetting.SORT_DATE)) {
            groupHolder.tvDate.setText(group.getInputDate());
        }else{
            groupHolder.tvDate.setText(group.getDepartment());
        }
        return v;
    }

    private StringBuilder getAllDoctors(ArrayList<String> allDoctors) {
        StringBuilder res = new StringBuilder();
        for (String doctor : allDoctors){
            res.append(doctor);
            res.append(", ");
        }
        return res;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
