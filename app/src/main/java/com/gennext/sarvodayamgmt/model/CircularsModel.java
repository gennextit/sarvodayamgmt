package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

/**
 * Created by Admin on 7/6/2017.
 */

public class CircularsModel {

    private String date;
    private String title;
    private String issuer;
    private String uploadedBy;
    private String output;
    private String outputMsg;
    private ArrayList<CircularsModel> list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<CircularsModel> getList() {
        return list;
    }

    public void setList(ArrayList<CircularsModel> list) {
        this.list = list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }
}
