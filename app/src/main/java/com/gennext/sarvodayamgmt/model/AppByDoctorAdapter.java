package com.gennext.sarvodayamgmt.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;

import java.util.ArrayList;

public class AppByDoctorAdapter extends RecyclerView.Adapter<AppByDoctorAdapter.ReyclerViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<AppointmentModel> items;

    public AppByDoctorAdapter(Activity context, ArrayList<AppointmentModel> items) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_app_by_doctor, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final AppointmentModel item = items.get(position);
        holder.tvAppId.setText(item.getAppId());
        holder.tvAppDate.setText(item.getAppDate());
        holder.tvAppTime.setText(item.getAppTime());


        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               shopListViewActivity.addFragment(ShopDetail.newInstance(context,item,mCurrentLocation),"shopDetail");
//                parentRef.startColorPicker(position,item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvAppId,tvAppDate, tvAppTime;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvAppId = (TextView) v.findViewById(R.id.tv_field_1);
            tvAppDate = (TextView) v.findViewById(R.id.tv_field_2);
            tvAppTime = (TextView) v.findViewById(R.id.tv_field_3); 
        }
    }
}

