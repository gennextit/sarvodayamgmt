package com.gennext.sarvodayamgmt.model;

/**
 * Created by Admin on 7/5/2017.
 */

public class DimensSettingModel {

    private int type;
    private String title;
    private int textSize;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }
}
