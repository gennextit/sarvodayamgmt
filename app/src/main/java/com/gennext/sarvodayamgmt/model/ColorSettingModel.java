package com.gennext.sarvodayamgmt.model;

/**
 * Created by Admin on 7/4/2017.
 */

public class ColorSettingModel {

    private int type;
    private String title;
    private int selectedColor;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
    }
}
