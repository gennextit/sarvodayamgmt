package com.gennext.sarvodayamgmt.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;

import java.util.ArrayList;

public class ErrorAdapter extends RecyclerView.Adapter<ErrorAdapter.MyViewHolder>{

    private final ArrayList<String> list;
    private LayoutInflater inflater;
    private Context context;

    public ErrorAdapter(Context context, ArrayList<String> list) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.list=list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.error_slot, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvDetail.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvDetail;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvDetail = (TextView)itemView.findViewById(R.id.tv_error_text);
        }
    }
}