package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

/**
 * Created by Admin on 1/10/2018.
 */

public class LoginModel {

    private String id;
    private String userId;
    private String email;
    private String password;
    private String createdAt;

    private ArrayList<LoginModel> list;
    private String output;
    private String outputMsg;

    public ArrayList<LoginModel> getList() {
        return list;
    }

    public void setList(ArrayList<LoginModel> list) {
        this.list = list;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
