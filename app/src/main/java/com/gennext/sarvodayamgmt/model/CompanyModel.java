package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

/**
 * Created by Admin on 10/3/2017.
 */

public class CompanyModel {
    private String id;
    private String name;
    private String email;
    private String password;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;


    public CompanyModel() {}

    public CompanyModel(String name, String email, String password) {
        this.name=name;
        this.email=email;
        this.password=password;
    }
//
    private ArrayList<CompanyModel> list;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<CompanyModel> getList() {
        return list;
    }

    public void setList(ArrayList<CompanyModel> list) {
        this.list = list;
    }

}
