package com.gennext.sarvodayamgmt.model;

import android.app.Activity;

import com.gennext.sarvodayamgmt.util.AppConfig;
import com.gennext.sarvodayamgmt.util.DateTimeUtility;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Admin on 8/10/2017.
 */

public class EXELMaker {

    private Activity mContext;

    public static EXELMaker newInstance(Activity mContext) {
        EXELMaker exelMaker = new EXELMaker();
        exelMaker.mContext = mContext;
        return exelMaker;
    }

    public File exportInExel(ArrayList<AppointmentModel> appList) {
        String fileName = "Appointment_List_" + DateTimeUtility.getTimeStamp() + ".xls";

        if (appList==null) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        sheet1 = wb.createSheet("Appointments");

        // Generate column headings
        Row row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 1, "Date");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "AppointmentId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 3, "time");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 4, "doctorId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 5, "doctorName");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 6, "department");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 7, "specialization");

        Row multiRow;
        int excelRowPos = 1;
        for (int rowPos = 0; rowPos < appList.size(); rowPos++) {
            // Generate column headings
            for (AppointmentModel model : appList.get(rowPos).getAppList()){
                multiRow = sheet1.createRow(excelRowPos);

                DoctorModel doctor = model.getDoctor();
                setRowEntry(cell, multiRow, 0, String.valueOf(excelRowPos));
                setRowEntry(cell, multiRow, 1, model.getAppDate());
                setRowEntry(cell, multiRow, 2, model.getAppId());
                setRowEntry(cell, multiRow, 3, model.getAppTime());
                setRowEntry(cell, multiRow, 4, doctor.getDoctorId());
                setRowEntry(cell, multiRow, 5, doctor.getDoctorName());
                setRowEntry(cell, multiRow, 6, doctor.getDepartment());
                setRowEntry(cell, multiRow, 7, doctor.getSpecialization());
                excelRowPos++;
            }

        }

        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }

    private Cell createHeaderRowColumn(Sheet sheet1, Cell cell, Row row, CellStyle cs, int width, int columnPosition, String columnName) {
        cell = row.createCell(columnPosition);
        cell.setCellValue(columnName);
        cell.setCellStyle(cs);
        sheet1.setColumnWidth(columnPosition, (15 * width));
        return cell;
    }

    private Cell setRowEntry(Cell cell, Row multiRow, int listPos, String listValue) {
        cell = multiRow.createCell(listPos);
        cell.setCellValue(listValue);
        return cell;
    }


}
