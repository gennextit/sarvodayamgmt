package com.gennext.sarvodayamgmt.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;

import java.util.ArrayList;

public class CircularsAdapter extends RecyclerView.Adapter<CircularsAdapter.ReyclerViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<CircularsModel> items;

    public CircularsAdapter(Activity context, ArrayList<CircularsModel> items) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_circular, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final CircularsModel item = items.get(position);

        holder.tvTitle.setText(item.getTitle());
        holder.tvDate.setText(item.getDate());
        holder.tvIssuer.setText(item.getIssuer());

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               shopListViewActivity.addFragment(ShopDetail.newInstance(context,item,mCurrentLocation),"shopDetail");
//                parentRef.startColorPicker(position,item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvIssuer,tvDate, tvTitle;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvIssuer = (TextView) v.findViewById(R.id.tv_slot_1);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_2);
            tvTitle = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }
}

