package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

public class AppointmentModel {


    private String appId; // or appNo
    private String appDate;
    private String appTime;
    private String inputDate;
    private String department;
    private String total;

    private String doctorId;
    private String doctorName;
    private String patientId;
    private String patientName;
    private String phone;
    private String receiptNo;
    private String amount;
    private String paymentStatus;
    private String callStatus;


    private DoctorModel doctor;
    private ArrayList<String> allDoctors;

    private String output;
    private String outputMsg;
    private ArrayList<AppointmentModel> dateList;
    private ArrayList<AppointmentModel> appList;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public ArrayList<String> getAllDoctors() {
        return allDoctors;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setAllDoctors(ArrayList<String> allDoctors) {
        this.allDoctors = allDoctors;
    }

    public DoctorModel getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorModel doctor) {
        this.doctor = doctor;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public AppointmentModel() {    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getAppTime() {
        return appTime;
    }

    public void setAppTime(String appTime) {
        this.appTime = appTime;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<AppointmentModel> getDateList() {
        return dateList;
    }

    public void setDateList(ArrayList<AppointmentModel> dateList) {
        this.dateList = dateList;
    }

    public ArrayList<AppointmentModel> getAppList() {
        return appList;
    }

    public void setAppList(ArrayList<AppointmentModel> appList) {
        this.appList = appList;
    }
}
