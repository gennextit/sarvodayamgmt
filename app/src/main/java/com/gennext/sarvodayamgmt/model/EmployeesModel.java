package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

/**
 * Created by Admin on 9/16/2017.
 */

public class EmployeesModel {

    private String id;
    private String companyId;
    private String empNo;
    private String userId;
    private String password;
    private String email;
    private String name;
    private String mobile;
    private String designation;
    private String image;
    private String lastSyncTime;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;

    public EmployeesModel(){}

    public EmployeesModel(String id, String companyId, String username, String password, String name, String mobile, String designation, String imagePath, String imageUrl, String email , String empNo){
        this.id = id;
        this.companyId = companyId;
        this.empNo = empNo;
        this.userId = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.mobile = mobile;
        this.designation = designation;
        this.image = imagePath;
    }

    public String getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(String lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    private ArrayList<EmployeesModel> list;

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<EmployeesModel> getList() {
        return list;
    }

    public void setList(ArrayList<EmployeesModel> list) {
        this.list = list;
    }
}
