package com.gennext.sarvodayamgmt.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.setting.DimensSetting;
import com.gennext.sarvodayamgmt.util.AppConfig;

import java.util.ArrayList;

public class DimensSettingAdapter extends RecyclerView.Adapter<DimensSettingAdapter.ReyclerViewHolder> {

    private static final int MIN_VAL = 14;
    private DimensSetting parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<DimensSettingModel> items;

    public DimensSettingAdapter(Activity context, ArrayList<DimensSettingModel> items, DimensSetting parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef=parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_setting_dimens, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final DimensSettingModel item = items.get(position);

        holder.tvTitle.setText(item.getTitle());
        holder.tvTextSize.setText(String.valueOf(item.getTextSize())+"sp");

        holder.sbSize.setMax(35);
        holder.sbSize.setProgress(item.getTextSize());
        holder.sbSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                if(item.getTextSize()<MIN_VAL){
//                    progress+=item.getTextSize();
//                }else {
//                    progress += MIN_VAL;
//                }
                AppConfig.setAppDimens(context,item.getType(),progress);
                holder.tvTextSize.setText(String.valueOf(progress)+"sp");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private SeekBar sbSize;
        private LinearLayout llSlot;
        private TextView tvTitle, tvTextSize;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.slot_main);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvTextSize = (TextView) v.findViewById(R.id.tv_selected_color);
            sbSize = (SeekBar) v.findViewById(R.id.seekBar);
        }
    }
}

