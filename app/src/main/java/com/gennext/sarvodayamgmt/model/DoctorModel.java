package com.gennext.sarvodayamgmt.model;

import java.util.ArrayList;

public class DoctorModel {


    private String doctorId;
    private String doctorName;
    private String department;
    private String specialization;
    private String degree;
    private String imageURL;

    private String output;
    private String outputMsg;
    private ArrayList<DoctorModel> doctorList;
    private ArrayList<DoctorModel> list;

    public ArrayList<DoctorModel> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(ArrayList<DoctorModel> doctorList) {
        this.doctorList = doctorList;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<DoctorModel> getList() {
        return list;
    }

    public void setList(ArrayList<DoctorModel> list) {
        this.list = list;
    }
}
