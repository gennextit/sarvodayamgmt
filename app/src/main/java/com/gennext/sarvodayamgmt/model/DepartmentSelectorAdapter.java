package com.gennext.sarvodayamgmt.model;

/**
 * Created by Abhijit on 14-Nov-16.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.pannel.DepartmentSelector;

import java.util.ArrayList;
import java.util.List;

public class DepartmentSelectorAdapter extends RecyclerView.Adapter<DepartmentSelectorAdapter.ReyclerViewHolder> {

    private final DepartmentSelector parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private List<DoctorModel> originalData;
    private List<DoctorModel> items;//filterData
    private ItemFilter mFilter = new ItemFilter();

    public DepartmentSelectorAdapter(Activity context, ArrayList<DoctorModel> items, DepartmentSelector parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.originalData = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_empty, parent, false);
        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final DoctorModel item = items.get(position);

        String stateName = item.getDepartment();
        if (!TextUtils.isEmpty(stateName)) {
            holder.tvName.setText(stateName);
        }
        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.onSelectedItem(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvName;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_message);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<DoctorModel> list = originalData;

            int count = list.size();
            final ArrayList<DoctorModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (DoctorModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getDepartment();
                        if (filterableText != null && filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<DoctorModel>) results.values;
            notifyDataSetChanged();
        }

    }

}
