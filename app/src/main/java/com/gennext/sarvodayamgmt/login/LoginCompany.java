package com.gennext.sarvodayamgmt.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gennext.sarvodayamgmt.CompanyActivity;
import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.CompanyModel;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.AppUser;
import com.gennext.sarvodayamgmt.util.DBHelper;
import com.gennext.sarvodayamgmt.util.FieldValidation;
import com.gennext.sarvodayamgmt.util.JsonParser;
import com.gennext.sarvodayamgmt.util.ProgressButtonRounded;


public class LoginCompany extends CompactFragment {

    private EditText etUsername, etPassword;
    private AssignTask assignTask;
    private ProgressButtonRounded btnAction;
    private LinearLayout llSignup;
    private DBHelper dbManager;

    public static LoginCompany newInstance() {
        LoginCompany fragment = new LoginCompany();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_login_company, container, false);
        initToolBarTheme(getActivity(), v, getString(R.string.company_login));
        dbManager = new DBHelper(getActivity());
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        etUsername = (EditText) v.findViewById(R.id.et_employee_username);
        etPassword = (EditText) v.findViewById(R.id.et_employee_password);
        llSignup = (LinearLayout) v.findViewById(R.id.ll_signup);

        btnAction = ProgressButtonRounded.newInstance(getContext(), v);

        btnAction.setText(getString(R.string.login));
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etUsername)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etPassword)) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etUsername.getText().toString(), etPassword.getText().toString());
            }
        });

        llSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(SignupCompany.newInstance(), "signupCompany");
            }
        });

        btnAction.setOnEditorActionListener(etPassword,"Login");

    }


    private void executeTask(String email, String password) {
        assignTask = new AssignTask(getActivity(), email, password);
        assignTask.execute(AppSettings.LOGIN_COMPANY);
    }





    private class AssignTask extends AsyncTask<String, Void, CompanyModel> {
        private final String email, password;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String email, String password) {
            this.email = email;
            this.password = password;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected CompanyModel doInBackground(String... urls) {
            String response = null;
//            response = ApiCall.POST(urls[0], RequestBuilder.companyLogin(email, password));
            return JsonParser.companyLogin(response);//return id=compId,data=name
//            return dbManager.companyLogin(new CompanyModel(null,email,password));
        }


        @Override
        protected void onPostExecute(CompanyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
                    AppUser.setCompanyId(context, result.getId());
                    AppUser.setName(context, result.getName());
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnAction.revertAnimation();
                    String[] errorSoon = {email, password};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, new ApiCallError.ErrorParamListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog, String[] param) {
                            executeTask(param[0], param[1]);
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog, String[] param) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
//                        if (manageProfile != null) {
//                            manageProfile.refreshList();
//                            activity.onBackPressed();
//                        } else {
                        Intent intent = new Intent(getActivity(), CompanyActivity.class);
                        startActivity(intent);
                        getActivity().finish();
//                        }
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}