package com.gennext.sarvodayamgmt.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.sarvodayamgmt.EmployeeActivity;
import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.CompanyModel;
import com.gennext.sarvodayamgmt.model.EmployeesModel;
import com.gennext.sarvodayamgmt.pannel.DoctorSelector;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCall;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.DBHelper;
import com.gennext.sarvodayamgmt.util.FieldValidation;
import com.gennext.sarvodayamgmt.util.JsonParser;
import com.gennext.sarvodayamgmt.util.ProgressButtonRounded;
import com.gennext.sarvodayamgmt.util.RequestBuilder;


public class LoginEmployee extends CompactFragment{

    private EditText etUsername,etPassword,etCompany;
    private AssignTask assignTask;
    private ProgressButtonRounded btnAction;
    private DBHelper dbManager;
    private String mCompanyId,mCompanyName;

    public static LoginEmployee newInstance() {
        LoginEmployee fragment = new LoginEmployee();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_login_employee, container, false);
        initToolBarTheme(getActivity(), v, getString(R.string.employee_login));
        dbManager= new DBHelper(getActivity());
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
 
        etUsername=(EditText)v.findViewById(R.id.et_employee_username);
        etPassword=(EditText)v.findViewById(R.id.et_employee_password);
        etCompany=(EditText)v.findViewById(R.id.et_employee_company);

        btnAction = ProgressButtonRounded.newInstance(getContext(), v);

        btnAction.setText(getString(R.string.login));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etUsername)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etPassword)) {
                    return;
                }
//                else if (!FieldValidation.isEmpty(getContext(), mCompanyId,"Invalid company")) {
//                    return;
//                }
                hideKeybord(getActivity());
                executeTask(etUsername.getText().toString(), etPassword.getText().toString(),mCompanyId);
            }
        });

        etCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DoctorSelector.newInstance(LoginEmployee.this).show(getFragmentManager(),"companySelector");
            }
        });

        etPassword.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
//                    DoctorSelector.newInstance(LoginEmployee.this).show(getFragmentManager(),"companySelector");
                    handled = true;
                }
                return handled;
            }
        });
    }



    private void executeTask(String username, String password, String companyId) {
        assignTask = new AssignTask(getActivity(),username,password,companyId);
        assignTask.execute(AppSettings.LOGIN_EMPLOYEE);
    }



    private class AssignTask extends AsyncTask<String, Void, EmployeesModel> {
        private final String username,password,companyId;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String username, String password, String companyId) {
            this.username=username;
            this.password=password;
            this.companyId=companyId;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected EmployeesModel doInBackground(String... urls) {
            String response = null;
            response = ApiCall.POST(urls[0], RequestBuilder.employeeLogin(username,password,companyId));
            return JsonParser.employeeLogin(response,dbManager);
        }


        @Override
        protected void onPostExecute(EmployeesModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
//                    AppUser.setLastSyncTime(context, "");
//                    AppUser.setUserId(context, result.getId());
//                    AppUser.setUserCompanyId(context, result.getCompanyId());
//                    AppUser.setName(context, result.getName());
//                    AppUser.setImage(context, result.getImage());
//                    AppUser.setLastSyncTime(context, result.getLastSyncTime());
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnAction.revertAnimation();
                    String[] errorSoon = {username,password,companyId};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, new ApiCallError.ErrorParamListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog, String[] param) {
                            executeTask(param[0], param[1], param[2]);
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog, String[] param) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
//                        if (manageProfile != null) {
//                            manageProfile.refreshList();
//                            activity.onBackPressed();
//                        } else {
                            Intent intent = new Intent(getActivity(), EmployeeActivity.class);
                            startActivity(intent);
                            getActivity().finish();
//                        }
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}