package com.gennext.sarvodayamgmt.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gennext.sarvodayamgmt.CompanyActivity;
import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.AppWebView;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.global.PopupAlert;
import com.gennext.sarvodayamgmt.model.CompanyModel;
import com.gennext.sarvodayamgmt.setting.Const;
import com.gennext.sarvodayamgmt.util.ApiCallError;
import com.gennext.sarvodayamgmt.util.AppAnimation;
import com.gennext.sarvodayamgmt.util.AppSettings;
import com.gennext.sarvodayamgmt.util.AppUser;
import com.gennext.sarvodayamgmt.util.DBHelper;
import com.gennext.sarvodayamgmt.util.FieldValidation;
import com.gennext.sarvodayamgmt.util.JsonParser;
import com.gennext.sarvodayamgmt.util.ProgressButtonRounded;


public class SignupCompany extends CompactFragment implements ApiCallError.ErrorParamListener{

    private EditText etName,etEmail,etPassword;
    private AssignTask assignTask;
    private ProgressButtonRounded btnSigninOtp;
    private LinearLayout llTearms;
    private DBHelper dbManager;

    public static SignupCompany newInstance() {
        SignupCompany fragment = new SignupCompany();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_signup_company, container, false);
        initToolBarTheme(getActivity(), v, "Company Signup");
        dbManager =new DBHelper(getActivity());
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        etName=(EditText)v.findViewById(R.id.et_company_name);
        etEmail=(EditText)v.findViewById(R.id.et_company_email);
        etPassword=(EditText)v.findViewById(R.id.et_company_password);

        llTearms=(LinearLayout)v.findViewById(R.id.ll_tearm_user);

        btnSigninOtp = ProgressButtonRounded.newInstance(getContext(), v);

        btnSigninOtp.setText("SignUp");

        btnSigninOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etName)) {
                    return;
                } else if (!FieldValidation.isEmail(getContext(), etEmail,true)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etPassword)) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etName.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
            }
        });

        llTearms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(AppWebView.newInstance("TEARMS OF USE",AppSettings.TERMS_OF_USE, AppWebView.URL), "appWebView");
            }
        });

        btnSigninOtp.setOnEditorActionListener(etPassword,"Signup");
    }



    private void executeTask(String name, String email, String password) {
        assignTask = new AssignTask(getActivity(),name,email,password);
        assignTask.execute(AppSettings.SIGNUP_COMPANY);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }


    private class AssignTask extends AsyncTask<String, Void, CompanyModel> {
        private final String name,email,password;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String name, String email, String password) {
            this.name=name;
            this.email=email;
            this.password=password;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnSigninOtp.startAnimation();
        }

        @Override
        protected CompanyModel doInBackground(String... urls) {
            String response = null;
//            response = ApiCall.POST(urls[0], RequestBuilder.companySignup(name,email,password));
            return JsonParser.companySignup(response);
//            return dbManager.companySignup(new CompanyModel(name,email,password));
        }


        @Override
        protected void onPostExecute(CompanyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
                    AppUser.setName(context, result.getName());
                    AppUser.setCompanyId(context, result.getId());
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnSigninOtp.revertAnimation();
                    PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnSigninOtp.revertAnimation();
                    String[] errorSoon = {name,password};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, SignupCompany.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
//                        if (manageProfile != null) {
//                            manageProfile.refreshList();
//                            activity.onBackPressed();
//                        } else {
                        Intent intent = new Intent(getActivity(), CompanyActivity.class);
                        startActivity(intent);
                        getActivity().finish();
//                        }
                    }
                }
            };

            btnSigninOtp.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}