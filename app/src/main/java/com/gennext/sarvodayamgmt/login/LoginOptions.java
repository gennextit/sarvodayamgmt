package com.gennext.sarvodayamgmt.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gennext.sarvodayamgmt.LoginActivity;
import com.gennext.sarvodayamgmt.R;
import com.gennext.sarvodayamgmt.global.AppWebView;
import com.gennext.sarvodayamgmt.global.CompactFragment;
import com.gennext.sarvodayamgmt.util.AppSettings;


public class LoginOptions extends CompactFragment implements View.OnClickListener{

    private LinearLayout btnEmployee,btnCompany;

    public static LoginOptions newInstance() {
        LoginOptions fragment = new LoginOptions();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_login_options, container, false);
        initToolBarForThemeActivity(getActivity(), v, getString(R.string.login));
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        btnEmployee=(LinearLayout) v.findViewById(R.id.ll_login_employee);
        btnCompany=(LinearLayout)v.findViewById(R.id.ll_login_company);
        LinearLayout llTearms = (LinearLayout) v.findViewById(R.id.ll_tearm_user);

        btnEmployee.setOnClickListener(this);
        btnCompany.setOnClickListener(this);
        llTearms.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_login_employee:
                ((LoginActivity)getActivity()).openLoginEmployeeOption();
                break;
            case R.id.ll_login_company:
                ((LoginActivity)getActivity()).openLoginCompanyOption();
                break;
            case R.id.ll_tearm_user:
                addFragment(AppWebView.newInstance(getString(R.string.terms_of_use), AppSettings.TERMS_OF_USE, AppWebView.URL), "appWebView");
                break;

        }
    }
}
