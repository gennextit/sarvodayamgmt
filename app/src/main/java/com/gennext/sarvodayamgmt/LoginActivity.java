package com.gennext.sarvodayamgmt;

import android.os.Bundle;

import com.gennext.sarvodayamgmt.login.LoginCompany;
import com.gennext.sarvodayamgmt.login.LoginEmployee;
import com.gennext.sarvodayamgmt.login.LoginOptions;


/**
 * Created by Admin on 5/22/2017.
 */

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addFragmentWithoutBackstack(LoginOptions.newInstance(), "loginOptions");
    }

    public void openLoginEmployeeOption() {
        addFragment(LoginEmployee.newInstance(), "loginEmployee");
    }

    public void openLoginCompanyOption() {
        addFragment(LoginCompany.newInstance(), "loginCompany");
    }
}