package com.gennext.sarvodayamgmt;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by Admin on 4/16/2018.
 */

public class BottomNavagationActivity  extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private TextView mTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navagation);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_menu_1:
                mTextMessage.setText(R.string.title_home);
                return true;
            case R.id.navigation_menu_2:
                mTextMessage.setText(R.string.title_dashboard);
                return true;
            case R.id.navigation_menu_3:
                mTextMessage.setText(R.string.title_notifications);
                return true;
        }
        return false;
    }
}