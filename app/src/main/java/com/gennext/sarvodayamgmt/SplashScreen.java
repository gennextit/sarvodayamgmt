package com.gennext.sarvodayamgmt;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.gennext.sarvodayamgmt.util.AppUser;

/**
 * Created by Abhijit-PC on 17-Mar-17.
 */

public class SplashScreen extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!AppUser.getUserId(this).equals("") || !AppUser.getCompanyId(this).equals("")){
            if(!AppUser.getUserId(this).equals("")){
                startActivity(new Intent(SplashScreen.this,EmployeeActivity.class));
                finish();
            }else if(!AppUser.getCompanyId(this).equals("")){
                startActivity(new Intent(SplashScreen.this,CompanyActivity.class));
                finish();
            }else{
                startActivity(new Intent(SplashScreen.this,LoginActivity.class));
                finish();
            }
        }else{
            startActivity(new Intent(SplashScreen.this,LoginActivity.class));
            finish();
        }
    }
}
